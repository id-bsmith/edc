﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Caching;

namespace IBV.EDC.Lib.Caching
{
    public static class CacheHelper
    {
        // CONSTANTS

        private const string CACHE_HELPER_KEY = "IBV.CacheHelper.";

        // METHODS

        public static void Add(string cacheKey, object obj)
        {
            if (HttpContext.Current != null)
            {
                int.TryParse(ConfigurationManager.AppSettings.Get("Cache.Duration").ToString(), out int cacheDuration);

                if (cacheDuration > 0)
                {
                    // Ensure the cache dependency exists before inserting item into the cache
                    object cacheHelper = HttpContext.Current.Cache.Get(CACHE_HELPER_KEY) as object;
                    if (cacheHelper == null)
                    {
                        HttpContext.Current.Cache.Insert(
                            CACHE_HELPER_KEY,
                            DateTime.Now,
                            null,
                            Cache.NoAbsoluteExpiration,
                            Cache.NoSlidingExpiration,
                            CacheItemPriority.NotRemovable,
                            null);
                    }

                    HttpContext.Current.Cache.Insert(
                        string.Concat(CACHE_HELPER_KEY, cacheKey),
                        obj,
                        new CacheDependency(null, new string[] { CACHE_HELPER_KEY }),
                        Cache.NoAbsoluteExpiration,
                        TimeSpan.FromMinutes(cacheDuration),
                        CacheItemPriority.Default,
                        null);
                }
            }
        }

        public static object Get(string cacheKey)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Cache.Get(string.Concat(CACHE_HELPER_KEY, cacheKey));
            }

            return null;
        }

        public static void Remove(string cacheKey)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Cache.Remove(string.Concat(CACHE_HELPER_KEY, cacheKey));
            }
        }

        public static void ClearCache()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Cache.Remove(CACHE_HELPER_KEY);
            }
        }
    }
}
