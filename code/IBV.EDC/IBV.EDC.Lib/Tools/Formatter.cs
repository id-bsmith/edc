﻿using System;

namespace IBV.EDC.Lib.Tools
{
    public static class Formatter
    {
        // METHODS

        public static string GetFormattedFileSize(long bytes)
        {
            if (bytes < 1024)
            {
                return string.Format("{0}B", bytes);
            }

            long kb = bytes / 1024;
            if (kb < 1024)
            {
                return string.Format("{0}KB", kb);
            }

            long mb = kb / 1024;
            return string.Format("{0}MB", mb);
        }

        public static string ReplaceLineBreaks(string input)
        {
            return input.Replace("\n", "<br />");
        }

        public static string GetDateTimeInIsoFormat(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        public static string GetSafeCsvString(string input, bool appendComma = true)
        {
            string output = input != null ? string.Format("\"{0}\"", input.Replace("\"", "\"\"")) : string.Empty;
            return (appendComma) ? string.Format("{0},", output) : output;
        }
    }
}
