﻿using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Tools
{
    public static class IPublishedContentTools
    {
        public static int SITE_NODE_PATH_LEVEL = 1;
        public static int HOME_NODE_PATH_LEVEL = 2;
        public static int MEDIA_SECTION_PATH_LEVEL = 1;



        public static UmbracoHelper GetUmbracoHelper()
        {
            string key = "UmbracoHelper";

            UmbracoHelper umbracoHelper = HttpContext.Current.Items[key] as UmbracoHelper;
            if(umbracoHelper == null)
            {
                umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            }

            return umbracoHelper;
        }



        public static IPublishedContent GetAncestorByLevel(IPublishedContent content, int level)
        {
            return content.AncestorsOrSelf<IPublishedContent>().Where(x => x.Level == level).FirstOrDefault();
        }

        public static IPublishedContent GetConfigurationNode()
        {
            string key = "ConfigurationNode";

            IPublishedContent configurationNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (configurationNode == null)
            {
                configurationNode = GetSettingsNode().Children().Where(x => x.DocumentTypeAlias == "configuration").FirstOrDefault();
            }

            return configurationNode;
        }

        public static IPublishedContent GetCurrentNode()
        {
            string key = "CurrentNode";

            IPublishedContent currentNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (currentNode == null)
            {
                UmbracoHelper umbracoHelper = GetUmbracoHelper();
                currentNode = umbracoHelper.AssignedContentItem;
                HttpContext.Current.Items[key] = currentNode;
            }

            return currentNode;
        }

        public static IPublishedContent GetEmailNode()
        {
            string key = "EmailNode";

            IPublishedContent configurationNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (configurationNode == null)
            {
                configurationNode = GetSettingsNode().Children().Where(x => x.DocumentTypeAlias == "email").FirstOrDefault();
            }

            return configurationNode;
        }

        public static IPublishedContent GetEmailNodeFromID(string PageID)
        {
            int pageID = 0;
            int.TryParse(PageID, out pageID);

            var uh = GetUmbracoHelper();
            var currentNode = uh.TypedContent(pageID);
            var homeNode = currentNode.Ancestor(1);
            var settingsNode = homeNode.Children(x => x.DocumentTypeAlias == "settings").FirstOrDefault();
            var emailNode = settingsNode.Children(x => x.DocumentTypeAlias == "email").FirstOrDefault();

            return emailNode;
        }

        public static IPublishedContent GetGlobalSettings()
        {
            string key = "GlobalSettings";

            IPublishedContent globalSettings = HttpContext.Current.Items[key] as IPublishedContent;
            if (globalSettings == null)
            {
                globalSettings = GetSiteNode().Siblings().Where(x => x.DocumentTypeAlias == "settings").FirstOrDefault();
            }

            return globalSettings;
        }

        public static IPublishedContent GetHomeNode()
        {
            string key = "HomeNode";

            IPublishedContent homeNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (homeNode == null)
            {
                homeNode = GetCurrentNode().AncestorsOrSelf().Where(x => x.Level == HOME_NODE_PATH_LEVEL).FirstOrDefault();
            }

            return homeNode;
        }

        public static IPublishedContent GetLayoutNode()
        {
            string key = "LayoutNode";

            IPublishedContent layoutNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (layoutNode == null)
            {
                layoutNode = GetSettingsNode().Children().Where(x => x.DocumentTypeAlias == "layout").FirstOrDefault();
            }

            return layoutNode;
        }

        public static IPublishedContent GetMediaSection(IPublishedContent media)
        {
            if (media?.Id > 0)
            {
                media = media.AncestorsOrSelf().Where(x => x.Level == MEDIA_SECTION_PATH_LEVEL).FirstOrDefault();
                if (media?.Id > 0)
                {
                    return media;
                }
            }

            return null;
        }

        public static IPublishedContent GetSettingsNode()
        {
            string key = "SettingsNode";

            IPublishedContent settingsNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (settingsNode == null)
            {
                settingsNode = GetHomeNode().Siblings().Where(x => x.DocumentTypeAlias == "settings").FirstOrDefault();
            }

            return settingsNode;
        }

        public static IPublishedContent GetSiteAlertNode()
        {
            string key = "SiteAlertNode";

            IPublishedContent siteAlertNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (siteAlertNode == null)
            {
                siteAlertNode = GetGlobalSettings().Children().Where(x => x.DocumentTypeAlias == "siteAlert").FirstOrDefault();
            }

            return siteAlertNode;
        }

        public static IPublishedContent GetSiteNode()
        {
            string key = "SiteNode";

            IPublishedContent siteNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (siteNode == null)
            {
                siteNode = GetCurrentNode().Ancestor(SITE_NODE_PATH_LEVEL);
                HttpContext.Current.Items[key] = siteNode;
            }

            return siteNode;
        }

        public static IPublishedContent GetSocialMediaNode()
        {
            string key = "SocialMediaNode";

            IPublishedContent socialMediaNode = HttpContext.Current.Items[key] as IPublishedContent;
            if (socialMediaNode == null)
            {
                socialMediaNode = GetSettingsNode().Children().Where(x => x.DocumentTypeAlias == "socialMedia").FirstOrDefault();
            }

            return socialMediaNode;
        }

        public static IPublishedContent GetUsefulLinksNode()
        {
            string key = "UsefulLinksNode";

            IPublishedContent usefulLinksNode = HttpContext.Current.Items[key] as IPublishedContent;
            if(usefulLinksNode == null)
            {
                usefulLinksNode = GetSiteNode().Children().Where(x => x.DocumentTypeAlias == "library").FirstOrDefault().Children().Where(x => x.DocumentTypeAlias == "usefulLinks").FirstOrDefault();
            }

            return usefulLinksNode;
        }


        public static string GetCanonical(IPublishedContent node)
        {
            if (node?.Id > 0)
            {
                string queryString = HttpContext.Current.Request.Url.Query;
                return (!string.IsNullOrWhiteSpace(queryString)) ? string.Format("{0}{1}", umbraco.library.NiceUrlWithDomain(node.Id), queryString) : umbraco.library.NiceUrlWithDomain(node.Id);
            }

            return null;
        }

        public static string GetMetaDescription(IPublishedContent node)
        {
            if (node?.Id > 0)
            {
                string metaDescription = node.GetProperty("metaDescription")?.Value.ToString();
                if (string.IsNullOrEmpty(metaDescription))
                {
                    // if meta description is empty, try node summary
                    metaDescription = node.GetProperty("nodeSummary")?.Value.ToString();
                }

                return metaDescription;
            }

            return null;
        }

        public static string GetMetaTitle(IPublishedContent node)
        {
            if (node?.Id > 0)
            {
                string metaTitle = node.GetProperty("metaTitle")?.Value.ToString(); // Use meta title if not empty
                if (string.IsNullOrEmpty(metaTitle))
                {
                    metaTitle = node.GetProperty("nodeLinkTitle")?.Value.ToString(); // Else use node link title
                    if (string.IsNullOrEmpty(metaTitle))
                    {
                        metaTitle = node.Name; // Else use node name
                    }
                }

                return metaTitle;
            }

            return null;
        }
    }
}
