﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Tools
{
    public static class ExamineTools
    {
        // CONSTANTS

        public const string DELIMITER = " LUCENEDELIMITER ";


        // METHODS

        public static string SanitizeStringForLucene(string query, IList<string> stopWords)
        {
            // Simple sanitisation to strip out stop words and prevent strings from starting with ? or * (which are used for Lucene wildcard searches)
            if (stopWords.Contains(query))
            {
                return string.Empty;
            }
            else if (query.StartsWith("?") || query.StartsWith("*"))
            {
                return string.Empty;
            }
            else
            {
                return query;
            }
        }

        public static string AddDelimiter(string input)
        {
            // The delimiter is used to allow exact text matches with Lucene (e.g. "foo" should not match "foo bar")
            return string.Format("{0}{1}{2}", DELIMITER, StripIncompatableQuotes(input), DELIMITER);
        }

        public static string RemoveDelimiter(string input)
        {
            return input.Substring(DELIMITER.Length, input.Length - (2 * DELIMITER.Length));
        }

        public static string StripIncompatableQuotes(string source)
        {
            return (!string.IsNullOrEmpty(source)) ? source.Replace('\u2018', '\'').Replace('\u2019', '\'').Replace('\u201c', '\"').Replace('\u201d', '\"') : source;
        }
    }
}
