﻿using Examine;
using IBV.EDC.Lib.Domain;
using IBV.EDC.Lib.Extensions;
using IBV.EDC.Lib.Tools;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Membership;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Routing;
using System.Net;

namespace IBV.EDC.Lib.Events
{
    public class ApplicationEvents : ApplicationEventHandler
    {

        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentLastChanceFinderResolver.Current.SetFinder(new PageNotFoundContentFinder());
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ExamineManager.Instance.IndexProviderCollection["ContentIndexer"].GatheringNodeData += ContentGatheringNodeDataHandler;
            ExamineManager.Instance.IndexProviderCollection["PDFIndexer"].GatheringNodeData += PDFGatheringNodeDataHandler;
            ExamineManager.Instance.IndexProviderCollection["UsefulLinksIndexer"].GatheringNodeData += UsefulLinksGatheringNodeDataHandler;
            ExamineManager.Instance.IndexProviderCollection["EventsIndexer"].GatheringNodeData += (sender, e) => EventsGatheringNodeDataHandler(sender, e);

            ExamineManager.Instance.IndexProviderCollection["ContentIndexer"].NodeIndexing += ContentNodeIndexingHandler;
            ExamineManager.Instance.IndexProviderCollection["PDFIndexer"].NodeIndexing += PDFNodeIndexingHandler;
            ExamineManager.Instance.IndexProviderCollection["UsefulLinksIndexer"].NodeIndexing += UsefulLinksNodeIndexingHandler;
            ExamineManager.Instance.IndexProviderCollection["EventsIndexer"].NodeIndexing += (sender, e) => EventsNodeIndexingHandler(sender, e);

            ContentService.Published += ContentServicePublished;
            ContentService.Trashed += ContentServiceTrashed;
            ContentService.Saved += AddNodePermissions;
            MediaService.Saved += MediaServiceSaved;
        }

        private void UsefulLinksGatheringNodeDataHandler(object sender, IndexingNodeDataEventArgs e)
        {
            if (e.Fields["nodeTypeAlias"].ToString() == "usefulLink")
            {
                var helper = new UmbracoHelper(ContextHelper.EnsureUmbracoContext());

                IPublishedContent content = helper.TypedContent(e.NodeId);
                if (content == null)
                {
                    content = ApplicationContext.Current.Services.ContentService.GetById(e.NodeId).ToPublishedContent();
                }

                if (content?.Id > 0)
                {
                    if (!e.Fields.ContainsKey("searchableGuids"))
                    {
                        var fieldValue = e.Fields.ContainsKey("nodeTags") ? string.Join(" ", e.Fields["nodeTags"].Split(',').Select(x => GuidUdi.Parse(x).Guid)) : string.Empty;
                        e.Fields.Add("searchableGuids", fieldValue);
                    }
                }
            }
        }

        private void ContentGatheringNodeDataHandler(object sender, IndexingNodeDataEventArgs e)
        {
            if (e.Fields.ContainsKey("template") && e.Fields["template"].ToString() != "0")
            {
                var helper = new UmbracoHelper(ContextHelper.EnsureUmbracoContext());

                IPublishedContent content = helper.TypedContent(e.NodeId);
                if (content == null)
                {
                    // Workaroud for when content is null.  Generally using ContentService is not
                    // recommended because it involves hitting the database but I don't see any way around this.

                    content = ApplicationContext.Current.Services.ContentService.GetById(e.NodeId).ToPublishedContent();
                }

                if (content?.Id > 0)
                {
                    // Add fields to content index
                    e.Fields.Add("nodeId", e.NodeId.ToString());
                    e.Fields.Add("parentNodeId", e.Fields["parentId"].ToString());

                    if (!e.Fields.ContainsKey("siteNodeId"))
                    {
                        e.Fields.Add("siteNodeId", content.Ancestor(1)?.Id.ToString() ?? string.Empty);
                    }

                    if (!e.Fields.ContainsKey("searchablePath"))
                    {
                        e.Fields.Add("searchablePath", content.Path.Replace(",", " "));
                    }

                    if (!e.Fields.ContainsKey("nodeTitle"))
                    {
                        var fieldValue = e.Fields.ContainsKey("nodeLinkTitle") && !string.IsNullOrEmpty(e.Fields["nodeLinkTitle"].ToString()) ? e.Fields["nodeLinkTitle"].ToString() : e.Fields["nodeName"].ToString();
                        e.Fields.Add("nodeTitle", fieldValue);
                    }

                    if (!e.Fields.ContainsKey("searchableGuids"))
                    {
                        var fieldValue = e.Fields.ContainsKey("nodeTags") ? string.Join(" ", e.Fields["nodeTags"].Split(',').Select(x => GuidUdi.Parse(x).Guid)) : string.Empty;
                        e.Fields.Add("searchableGuids", fieldValue);
                    }
                }
            }
        }

        private void PDFGatheringNodeDataHandler(object sender, IndexingNodeDataEventArgs e)
        {
            var helper = new UmbracoHelper(ContextHelper.EnsureUmbracoContext());

            IPublishedContent media = (helper != null) ? helper.TypedMedia(e.NodeId) : null;
            if (media != null && media.Id > 0 && media.Parent?.Id != -21)
            {
                // Add fields to PDF index
                e.Fields.Add("nodeId", e.NodeId.ToString());
                e.Fields.Add("parentNodeId", media.Parent != null ? media.Parent.Id.ToString() : "");

                string fieldValue;
                if (!e.Fields.ContainsKey("siteNodeId"))
                {
                    IPublishedContent mediaSection = IPublishedContentTools.GetMediaSection(media);
                    e.Fields.Add("siteNodeId", mediaSection?.GetProperty("siteNode") != null ? mediaSection.GetPropertyValue<string>("siteNode") : string.Empty);
                }

                if (!e.Fields.ContainsKey("searchablePath"))
                {
                    e.Fields.Add("searchablePath", media.Path.Replace(",", " "));
                }

                if (!e.Fields.ContainsKey("nodeTitle"))
                {
                    string umbracoBytes = e.Fields.ContainsKey("umbracoBytes") ? e.Fields["umbracoBytes"].ToString() : "";
                    long.TryParse(umbracoBytes, out long size);
                    string type = e.Fields.ContainsKey("umbracoExtension") ? e.Fields["umbracoExtension"].ToString() : "";

                    e.Fields.Add("nodeTitle", $"{media.Name} ({Formatter.GetFormattedFileSize(size)}, {type})");
                }

                if (!e.Fields.ContainsKey("nodeAuthor"))
                {
                    fieldValue = media.HasValue("nodeAuthor") ? media.GetPropertyValue<string>("nodeAuthor") : string.Empty;
                    e.Fields.Add("nodeAuthor", fieldValue);
                }

                if (!e.Fields.ContainsKey("nodeImage"))
                {
                    fieldValue = (media.HasValue("nodeImage")) ? media.GetPropertyValue<string>("nodeImage") : string.Empty;
                    e.Fields.Add("nodeImage", fieldValue);
                }

                if (!e.Fields.ContainsKey("nodeSummary"))
                {
                    fieldValue = media.HasValue("nodeSummary") ? media.GetPropertyValue<string>("nodeSummary") : string.Empty;
                    e.Fields.Add("nodeSummary", fieldValue);
                }

                if (!e.Fields.ContainsKey("searchableGuids"))
                {
                    fieldValue = e.Fields.ContainsKey("nodeTags") ? string.Join(" ", e.Fields["nodeTags"].Split(',').Select(x => GuidUdi.Parse(x).Guid)) : string.Empty;
                    e.Fields.Add("searchableGuids", fieldValue);
                }
            }
        }

        private void EventsGatheringNodeDataHandler(object sender, IndexingNodeDataEventArgs e)
        {
            if ((e.Fields["nodeTypeAlias"].ToString() == "event") || (e.Fields["nodeTypeAlias"].ToString() == "eventRecurring"))
            {
                var helper = new UmbracoHelper(ContextHelper.EnsureUmbracoContext());

                IPublishedContent content = helper.TypedContent(e.NodeId);
                if (content == null)
                {
                    content = ApplicationContext.Current.Services.ContentService.GetById(e.NodeId).ToPublishedContent();
                }

                if (content?.Id > 0)
                {
                    if (!e.Fields.ContainsKey("searchableGuids"))
                    {
                        var fieldValue = e.Fields.ContainsKey("nodeTags") ? string.Join(" ", e.Fields["nodeTags"].Split(',').Select(x => GuidUdi.Parse(x).Guid)) : string.Empty;
                        e.Fields.Add("searchableGuids", fieldValue);
                    }
                }
            }
        }

        private void ContentNodeIndexingHandler(object sender, IndexingNodeEventArgs e)
        {
            // Do not index content nodes that do not have an associated template
            e.Cancel = (!e.Fields.ContainsKey("template") || e.Fields["template"].ToString() == "0");
        }

        private void PDFNodeIndexingHandler(object sender, IndexingNodeEventArgs e)
        {
            var helper = new UmbracoHelper(ContextHelper.EnsureUmbracoContext());

            // Do not index invalid media nodes or media nodes that have been deleted
            var media = helper != null ? helper.TypedMedia(e.NodeId): null;
            e.Cancel = (media == null || media.Id <= 0 || media.Parent?.Id == -21);
        }

        private void EventsNodeIndexingHandler(object sender, IndexingNodeEventArgs e)
        {
            // Do not index content nodes that do not have an associated template
            //e.Cancel = (!(e.Fields["nodeTypeAlias"].ToString() == "event") || (e.Fields["nodeTypeAlias"].ToString() == "eventRecurring"));
        }

        private void UsefulLinksNodeIndexingHandler(object sender, IndexingNodeEventArgs e)
        {
            // Do not index content nodes that do not have an associated template
            e.Cancel = !(e.Fields["nodeTypeAlias"].ToString() == "usefulLink");
        }

        private void ContentServicePublished(IPublishingStrategy sender, PublishEventArgs<IContent> args)
        {
            IBV.EDC.Lib.Caching.CacheHelper.ClearCache();

            foreach (var node in args.PublishedEntities)
            {
                if ((node.ContentType.Alias == "websitePrimary" ||
                    node.ContentType.Alias == "websiteSecondary" ||
                    node.ContentType.Alias == "websiteNursery" ||
                    node.ContentType.Alias == "websiteSpecial")
                    && node.WasPropertyDirty("Id"))
                {
                    new ScaffoldWebsite(node);
                }

                if (node.ContentType.Alias == "eventArticle" && node.WasPropertyDirty("Id"))
                {
                    var contentService = ApplicationContext.Current.Services.ContentService;
                    var content = contentService.GetById(node.Id);
                    content.SetValue("umbracoNaviHide", 1);
                    content.SetValue("hideFromSideMenu", 1);

                    contentService.SaveAndPublishWithStatus(content);
                }
            }
        }

        private void ContentServiceTrashed(IContentService sender, MoveEventArgs<IContent> e)
        {
            IBV.EDC.Lib.Caching.CacheHelper.ClearCache();
        }

        private void MediaServiceSaved(IMediaService sender, SaveEventArgs<IMedia> e)
        {
            IBV.EDC.Lib.Caching.CacheHelper.ClearCache();
        }
        private void AddNodePermissions(IContentService sender, SaveEventArgs<IContent> args)
        {
            foreach (var node in args.SavedEntities)
            {
                // We only need to do this when it's saved for the first time.  Therefore, if no Id exists then WasPropertyDirty will be true.
                var isNew = node.WasPropertyDirty("Id");
                if (isNew && node.Parent() != null)
                {
                    // Get permissions of the new nodes parent and copy them to the new node.
                    var parentPermissions = sender.GetPermissionsForEntity(node.Parent());
                    var permissions = new EntityPermissionSet(node.Id, parentPermissions);

                    sender.ReplaceContentPermissions(permissions);
                    sender.SaveAndPublishWithStatus(node);
                }
            }
        }
    }
}