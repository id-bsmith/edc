﻿using Examine;
using IBV.EDC.Lib.Models;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Domain
{
    public class ExamineEventsSearch
    {
        public IEnumerable<Examine.SearchResult> SearchResults { get; private set; }
        public List<string> ContentSearchFields { get; set; }
        private IEnumerable<Examine.SearchResult> _examineSearchResults = new List<Examine.SearchResult>();
        IFilterList _filterList = null;

        public IEnumerable<Examine.SearchResult> GetSearchResults(int pageID, string tagFilters)
        {
            if (!string.IsNullOrEmpty(tagFilters))
            {
                List<string> ids = tagFilters.Split(',').ToList();

                var pageId = IPublishedContentTools.GetUmbracoHelper().TypedContent(pageID);
                var tagProp = pageId.GetPropertyValue<IEnumerable<IPublishedContent>>("userFilters");
                _filterList = FilterList.Get(tagProp);

                foreach (var item in _filterList.Filters[0].FilterTags)
                {
                    if (ids.Contains(item.Tag.Id.ToString()))
                    {
                        item.IsSelected = true;
                    }
                }
            }

            _examineSearchResults = _examineSearchResults.Concat(GetEventSearchResults(pageID));

            return _examineSearchResults;
        }


        private List<Examine.SearchResult> GetEventSearchResults(int pageID)
        {
            var contentCriteria = ExamineManager.Instance
                .SearchProviderCollection["EventsSearcher"]
                .CreateSearchCriteria();

            try
            {
                // Add user filters criteria
                // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
                if (_filterList != null)
                {
                    if (_filterList.Filters.Count > 0)
                    {
                        StringBuilder userFilterBuilder = new StringBuilder();

                        // Loop through user filters. Note: User filter groups should be AND-ed.
                        foreach (Filter filter in _filterList.Filters)
                        {
                            // Loop through values for filter. Note: User filter values should be OR-ed within single filter group.
                            bool containsTags = false;
                            foreach (IFilterTag filterTag in filter.FilterTags)
                            {
                                if (filterTag.IsSelected)
                                {
                                    if (!containsTags)
                                    {
                                        userFilterBuilder.Append("+searchableGuids: (");
                                        containsTags = true;
                                    }

                                    userFilterBuilder.Append(" ");
                                    userFilterBuilder.Append(filterTag.Tag.Guid);
                                }
                            }

                            if (containsTags)
                            {
                                userFilterBuilder.Append(")");
                            }
                        }

                        if (userFilterBuilder.Length > 0)
                        {
                            contentCriteria = contentCriteria.RawQuery(userFilterBuilder.ToString());
                        }
                    }
                }

                var contentFilter = contentCriteria.Field("__IndexType", "content");

                this.ContentSearchFields = new List<string> { "nodeName", "eventTitle", "eventStartDateTime", "eventEndDateTime", "eventDetails", "eventArticleLink", "eventHideTime" };
                this.SearchResults = ExamineManager.Instance
                            .SearchProviderCollection["EventsSearcher"]
                            .Search(contentFilter.Compile().ParentId(pageID).Compile()).ToList<Examine.SearchResult>();
            }
            catch (NullReferenceException) // Standard analyzer will throw an exception if search contains only ignored keywords
            {
                this.SearchResults = null;
                //throw new ExamineSearchException("Search contains only ignored keywords.");
            }
            catch (Exception)
            {
                this.SearchResults = null;
            }

            return this.SearchResults.ToList();
        }
    }
}
