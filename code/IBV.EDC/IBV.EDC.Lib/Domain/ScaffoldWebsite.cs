﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Membership;
using Umbraco.Core.Services;

namespace IBV.EDC.Lib.Domain
{
    public class ScaffoldWebsite
    {
        // Each school type contains it's own specific pages to scaffold but a lot of the nodes created are used across
        // all or some of the different school types.  These nodes are created using the 'common' methods and any additional
        // ones specific to the school type are added afterwards.



        // TODO:
        // This class has become a bit of a mess
        // This will need to be refactored in to separate classes as it has became pretty large and will be difficult to main

        IContentService contentService = null;
        List<IContent> schoolEditorContentPages = new List<IContent>();
        List<IContent> configureNodes = new List<IContent>();
        List<IContent> headerExtLinks = new List<IContent>();
        IContent newsNode = null;
        IContent calendarNode = null;
        IContent upcomingEventsPanelNode = null;
        IContent latestNewsPanelNode = null;
        List<IContent> listNodes = new List<IContent>();

        public ScaffoldWebsite(IContent siteNode)
        {
            contentService = ApplicationContext.Current.Services.ContentService;
            var homeNode = contentService.CreateContentWithIdentity(siteNode.Name, siteNode, "home");

            switch (siteNode.ContentType.Alias)
            {
                case "websitePrimary":
                    CreatePrimarySite(homeNode); break;
                case "websiteSecondary":
                    CreateSecondarySite(homeNode); break;
                case "websiteNursery":
                    CreateNurserySite(homeNode); break;
                case "websiteSpecial":
                    CreateSpecialSite(homeNode); break;
            }

            CreateLibrary(siteNode);
            CreateSettings(siteNode);
            CreateUserGroupPermissions(siteNode);

            homeNode.SetValue("latestNews", latestNewsPanelNode.GetUdi().ToString());
            homeNode.SetValue("upcomingEvents", upcomingEventsPanelNode.GetUdi().ToString());
            contentService.Save(homeNode);
        }


        private void CreatePrimarySite(IContent homeNode)
        {
            CreateSchoolInfoNodes(homeNode);
            CreateLearningNodes(homeNode);
            CreatePrimaryNurseryNodes(homeNode);
            CreateNewsNodes(homeNode);
            CreateEventsNodes(homeNode);
            CreatePupilZoneNodes(homeNode);

            var parentInfoNode = CreateParentInfoNodes(homeNode);
            contentService.CreateContentWithIdentity("Parent Teacher Council", parentInfoNode, "generalArticleAlternative");

            CreateGetInTouchNodes(homeNode);
            CreateSearchNode(homeNode);
            CreateRootMiscNodes(homeNode);
        }

        private void CreateSecondarySite(IContent homeNode)
        {
            CreateSchoolInfoNodes(homeNode);
            CreateSecondaryLearningNodes(homeNode);

            var newsNode = CreateNewsNodes(homeNode);
            contentService.CreateContentWithIdentity("Achievements", newsNode, "generalArticle");
            contentService.CreateContentWithIdentity("Community / Charity Work", newsNode, "generalArticle");

            CreateEventsNodes(homeNode);

            var pupilZoneNode = CreatePupilZoneNodes(homeNode);
            contentService.CreateContentWithIdentity("Pupil Council", pupilZoneNode, "generalArticle");
            contentService.CreateContentWithIdentity("Transitions", pupilZoneNode, "generalArticle");
            contentService.CreateContentWithIdentity("Careers", pupilZoneNode, "generalArticle");
            contentService.CreateContentWithIdentity("School Captaincy Team", pupilZoneNode, "generalArticle");

            var parentInfoNode = CreateParentInfoNodes(homeNode);
            contentService.CreateContentWithIdentity("Parent Council", parentInfoNode, "generalArticle");

            var usefulLinksNode = contentService.CreateContentWithIdentity("Useful Links", parentInfoNode, "listUsefulLinks");
            var usefulLinksNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "UsefulLinks"),
                new KeyValuePair<string, string>("searchScope", "Site"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(usefulLinksNode, usefulLinksNodePropertyValues);

            CreateGetInTouchNodes(homeNode);
            CreateSearchNode(homeNode);
            CreateRootMiscNodes(homeNode);
        }

        private void CreateNurserySite(IContent homeNode)
        {
            CreateSchoolInfoNodes(homeNode);
            CreateNurseryLearningNodes(homeNode);
            CreateNewsNodes(homeNode);
            CreateEventsNodes(homeNode);
            CreateParentInfoNodes(homeNode);
            CreateGetInTouchNodes(homeNode);
            CreateSearchNode(homeNode);
            CreateRootMiscNodes(homeNode);
        }

        private void CreateSpecialSite(IContent homeNode)
        {
            CreateSchoolInfoNodes(homeNode);
            CreateLearningNodes(homeNode);

            var newsNode = CreateNewsNodes(homeNode);
            contentService.CreateContentWithIdentity("Gallery", newsNode, "generalArticle");

            CreateEventsNodes(homeNode);

            var pupilZoneNode = CreatePupilZoneNodes(homeNode);
            contentService.CreateContentWithIdentity("Pupil Handbook", pupilZoneNode, "generalArticleAlternative");

            var parentInfoNode = CreateParentInfoNodes(homeNode);
            contentService.CreateContentWithIdentity("Parent Teacher Council", parentInfoNode, "generalArticle");
            contentService.CreateContentWithIdentity("E-journals", parentInfoNode, "generalArticle");

            CreateGetInTouchNodes(homeNode);
            CreateSearchNode(homeNode);
            CreateRootMiscNodes(homeNode);
        }

        private void CreateLibrary(IContent siteNode)
        {
            var libraryNode = contentService.CreateContentWithIdentity("Library", siteNode, "library");

            var tagRootNode = contentService.CreateContentWithIdentity("Tags", libraryNode, "tags");
            InitTags(tagRootNode);

            contentService.CreateContentWithIdentity("FAQ Panels", libraryNode, "faqPanels");

            var resultPanelNode = contentService.CreateContentWithIdentity("Result Panels", libraryNode, "resultPanels");

            latestNewsPanelNode = contentService.CreateContentWithIdentity("Latest News", resultPanelNode, "resultPanel");
            var latestNewsNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "Past"),
                new KeyValuePair<string, string>("searchOrder", "Latest"),
                new KeyValuePair<string, string>("searchPageSize", "3"),
                new KeyValuePair<string, string>("resultsHeading", "Latest news"),
                new KeyValuePair<string, string>("resultsLinkText", "View all results")
            };
            latestNewsPanelNode.SetValue("resultsLink", newsNode.GetUdi().ToString());


            upcomingEventsPanelNode = contentService.CreateContentWithIdentity("Events", resultPanelNode, "resultPanel");
            var eventsNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Events"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "Past"),
                new KeyValuePair<string, string>("searchOrder", "Latest"),
                new KeyValuePair<string, string>("searchPageSize", "4"),
                new KeyValuePair<string, string>("resultsHeading", "Upcoming events"),
                new KeyValuePair<string, string>("resultsLinkText", "View all results")
            };
            upcomingEventsPanelNode.SetValue("resultsLink", calendarNode.GetUdi().ToString());

            InitNodeProps(latestNewsPanelNode, latestNewsNodePropertyValues);
            InitNodeProps(upcomingEventsPanelNode, eventsNodePropertyValues);

            var featureNode = contentService.CreateContentWithIdentity("Features", libraryNode, "features");
            var externalLinkNode = contentService.CreateContentWithIdentity("External Links", libraryNode, "externalLinks");
            var widgetsNode = contentService.CreateContentWithIdentity("Widgets", libraryNode, "widgets");

            if (siteNode.ContentType.Alias != "websiteNursery")
            {
                var parentPortalExtLinkNode = contentService.CreateContentWithIdentity("Parents Portal", externalLinkNode, "externalLink");
                InitNodeProps(parentPortalExtLinkNode, new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>("linkUrl", "https://parentportal.eschooldata.com") });
                headerExtLinks.Add(parentPortalExtLinkNode);

                var glowExtLinkNode = contentService.CreateContentWithIdentity("Glow", externalLinkNode, "externalLink");
                InitNodeProps(glowExtLinkNode, new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>("linkUrl", "https://glow.rmunify.com") });
                headerExtLinks.Add(glowExtLinkNode);
            }

            if(siteNode.ContentType.Alias == "websiteSecondary")
            {
                contentService.CreateContentWithIdentity("Useful Links", libraryNode, "usefulLinks");
            }

            contentService.PublishWithChildrenWithStatus(libraryNode, includeUnpublished: true);
        }

        private void CreateSettings(IContent siteNode)
        {
            var settingsNode = contentService.CreateContentWithIdentity("Settings", siteNode, "settings");

            var configuration = contentService.CreateContentWithIdentity("Configuration", settingsNode, "configuration");
            InitConfigurationNode(configuration);

            var layoutNode = contentService.CreateContentWithIdentity("Layout", settingsNode, "layout");
            InitLayoutNode(layoutNode);

            var socialMediaNode = contentService.CreateContentWithIdentity("Social Media", settingsNode, "socialMedia");
            var emailNode = contentService.CreateContentWithIdentity("Emails", settingsNode, "email");

            contentService.PublishWithChildrenWithStatus(settingsNode, includeUnpublished: true);
        }


        // COMMON
        private void CreateSchoolInfoNodes(IContent homeNode)
        {
            var schoolInfoNode = contentService.CreateContentWithIdentity("School Info", homeNode, "gateway");
            var schoolInfoNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(schoolInfoNode, schoolInfoNodePropertyValues);

            string[] pageNames = new string[] {"Handbook", "Our Staff", "Holidays", "School Hours", "Transport", "School Meals",
                            "Vision Values and Aims", "Uniforms", "Inspection Reports",
                            "Standards & Quality Reports" };
            foreach (var pageName in pageNames)
            {
                contentService.CreateContentWithIdentity(pageName, schoolInfoNode, "generalArticle");
            }

            string[] listPageNames = new string[] { "Improvement Plans", "Policies" };
            foreach (var listPageName in listPageNames)
            {
                var listDocNodePropertyValues = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("searchType", "Pdfs"),
                    new KeyValuePair<string, string>("searchScope", "Site"),
                    new KeyValuePair<string, string>("searchPeriod", "All"),
                    new KeyValuePair<string, string>("searchOrder", "None"),
                    new KeyValuePair<string, string>("searchPageSize", "10"),
                };

                var listDocNode = contentService.CreateContentWithIdentity(listPageName, schoolInfoNode, "listDocument");
                InitNodeProps(listDocNode, listDocNodePropertyValues);
                listNodes.Add(listDocNode);
            }
        }

        private void CreateLearningNodes(IContent homeNode)
        {
            var learningNode = contentService.CreateContentWithIdentity("Learning", homeNode, "gateway");
            var learningNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(learningNode, learningNodePropertyValues);

            var classesNode = contentService.CreateContentWithIdentity("Classes", learningNode, "generalArticle");
            schoolEditorContentPages.Add(classesNode);

            string[] pageNames = new string[] { "Curriculum", "Family Learning" };
            foreach (var pageName in pageNames)
            {
                contentService.CreateContentWithIdentity(pageName, learningNode, "generalArticle");
            }
        }

        private IContent CreateNewsNodes(IContent homeNode)
        {
            newsNode = contentService.CreateContentWithIdentity("News", homeNode, "listNews");
            var newsletterNode = contentService.CreateContentWithIdentity("Newsletters", newsNode, "listDocument");

            var newsNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "Latest"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            var newsletterNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Pdfs"),
                new KeyValuePair<string, string>("searchScope", "Site"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(newsNode, newsNodePropertyValues);
            schoolEditorContentPages.Add(newsNode);

            InitNodeProps(newsletterNode, newsletterNodePropertyValues);
            listNodes.Add(newsletterNode);

            return newsNode;
        }

        private void CreateEventsNodes(IContent homeNode)
        {
            var eventsNode = contentService.CreateContentWithIdentity("Events", homeNode, "listEvent");

            calendarNode = contentService.CreateContentWithIdentity("School Calendar", eventsNode, "calendar");
            contentService.CreateContentWithIdentity("Event Details", eventsNode, "generalArticle");

            var eventsNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "Future"),
                new KeyValuePair<string, string>("searchOrder", "Latest"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(eventsNode, eventsNodePropertyValues);
            schoolEditorContentPages.Add(calendarNode);
        }

        private void CreateGetInTouchNodes(IContent homeNode)
        {
            contentService.CreateContentWithIdentity("Get In Touch", homeNode, "contactForm");
        }

        private IContent CreateParentInfoNodes(IContent homeNode)
        {
            var parentsInfoNode = contentService.CreateContentWithIdentity("Parents Info", homeNode, "gateway");
            var parentInfoNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(parentsInfoNode, parentInfoNodePropertyValues);


            string[] listPageNames = new string[] { "Letters to Parents", "Forms" };
            foreach (var listPage in listPageNames)
            {
                var listDocNodePropertyValues = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("searchType", "Pdfs"),
                    new KeyValuePair<string, string>("searchScope", "Site"),
                    new KeyValuePair<string, string>("searchPeriod", "All"),
                    new KeyValuePair<string, string>("searchOrder", "None"),
                    new KeyValuePair<string, string>("searchPageSize", "10"),
                };

                var listDocNode = contentService.CreateContentWithIdentity(listPage, parentsInfoNode, "listDocument");
                InitNodeProps(listDocNode, listDocNodePropertyValues);
                listNodes.Add(listDocNode);
            }


            string[] pageNames = new string[] { "School Calendar", "Uniforms", "Planned Trips", "PTA", "Surveys" };
            foreach (var pageName in pageNames)
            {
                contentService.CreateContentWithIdentity(pageName, parentsInfoNode, "generalArticleAlternative");
            }

            contentService.CreateContentWithIdentity("FAQs", parentsInfoNode, "frequentlyAskedQuestions");

            return parentsInfoNode;
        }

        private IContent CreatePupilZoneNodes(IContent homeNode)
        {
            var pupilZoneNode = contentService.CreateContentWithIdentity("Pupil Zone", homeNode, "gateway");
            var pupilZoneNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(pupilZoneNode, pupilZoneNodePropertyValues);

            string[] pageNames = new string[] { "Clubs & Activities", "Support for Health", "School Trips" };
            foreach (var pageName in pageNames)
            {
                contentService.CreateContentWithIdentity(pageName, pupilZoneNode, "generalArticleAlternative");
            }

            return pupilZoneNode;
        }

        private void CreateSearchNode(IContent homeNode)
        {
            var searchNode = contentService.CreateContentWithIdentity("Search", homeNode, "listSearch");

            configureNodes.Add(searchNode);

            var searchNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Site"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "Relevancy"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            searchNode.SetValue("umbracoNaviHide", 1);
            searchNode.SetValue("showUserKeywordFilter", 1);
            searchNode.SetValue("hideFromSearch", 1);

            InitNodeProps(searchNode, searchNodePropertyValues);
        }

        private void CreateRootMiscNodes(IContent homeNode)
        {
            var error400Node = contentService.CreateContentWithIdentity("400", homeNode, "Error404");
            var cookieNode = contentService.CreateContentWithIdentity("Cookie Policy", homeNode, "generalArticle");

            error400Node.SetValue("umbracoNaviHide", 1);
            cookieNode.SetValue("umbracoNaviHide", 1);

            contentService.Save(error400Node);
            contentService.Save(cookieNode);

            configureNodes.Add(error400Node);
            configureNodes.Add(cookieNode);
        }


        // SPECIFIC SCHOOL TYPE NODES
        private void CreatePrimaryNurseryNodes(IContent homeNode)
        {
            var nurseryNode = contentService.CreateContentWithIdentity("Nursery", homeNode, "gateway");
            var nurseryNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(nurseryNode, nurseryNodePropertyValues);


            var galleriesNode = contentService.CreateContentWithIdentity("Galleries", nurseryNode, "generalArticle");
            schoolEditorContentPages.Add(galleriesNode);

            string[] pageNames = new string[] { "Additional Hours Entitlement", "Calendar", "Nursery Handbook",
                                                     "Improvement Plan", "Uniform", "Parent Info", "This week in the Nursery", "Surveys" };

            foreach (var pageName in pageNames)
            {
                contentService.CreateContentWithIdentity(pageName, nurseryNode, "generalArticle");
            }

            string[] listPageNames = new string[] { "Newsletters", "Policies" };
            foreach (var listPage in listPageNames)
            {
                var listDocNodePropertyValues = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("searchType", "Pdfs"),
                    new KeyValuePair<string, string>("searchScope", "Site"),
                    new KeyValuePair<string, string>("searchPeriod", "All"),
                    new KeyValuePair<string, string>("searchOrder", "None"),
                    new KeyValuePair<string, string>("searchPageSize", "10"),
                };

                var listDocNode = contentService.CreateContentWithIdentity(listPage, nurseryNode, "listDocument");
                InitNodeProps(listDocNode, listDocNodePropertyValues);
                listNodes.Add(listDocNode);
            }
        }

        private void CreateSecondaryLearningNodes(IContent homeNode)
        {
            var learningNode = contentService.CreateContentWithIdentity("Learning", homeNode, "gateway");
            var learningNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(learningNode, learningNodePropertyValues);

            var departmentNode = contentService.CreateContentWithIdentity("Departments", learningNode, "generalArticle");
            schoolEditorContentPages.Add(departmentNode);

            string[] pageNames = new string[] { "Curriculum", "SQA", "Exam Timetable", "Subject Options",
                                                    "Family Learning", "Guidance / Pastoral Care", "Library", "Study Skills" };

            foreach (var pageName in pageNames)
            {
                contentService.CreateContentWithIdentity(pageName, learningNode, "generalArticle");
            }
        }

        private void CreateNurseryLearningNodes(IContent homeNode)
        {
            var learningNode = contentService.CreateContentWithIdentity("Learning", homeNode, "gateway");
            var learningNodePropertyValues = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("searchType", "Content"),
                new KeyValuePair<string, string>("searchScope", "Children"),
                new KeyValuePair<string, string>("searchPeriod", "All"),
                new KeyValuePair<string, string>("searchOrder", "None"),
                new KeyValuePair<string, string>("searchPageSize", "10"),
            };

            InitNodeProps(learningNode, learningNodePropertyValues);

            string[] pageNames = new string[] { "Groups", "Pre-school", "Family Learning" };

            foreach (var pageName in pageNames)
            {
                contentService.CreateContentWithIdentity(pageName, learningNode, "generalArticle");
            }
        }


        // SET PROPERTY VALUES
        private void InitNodeProps(IContent listNode, List<KeyValuePair<string, string>> props)
        {
            foreach (var prop in props)
            {
                if (listNode.HasProperty(prop.Key))
                {
                    listNode.SetValue(prop.Key, prop.Value);
                }
            }

            contentService.Save(listNode);
        }

        private void InitConfigurationNode(IContent configNode)
        {
            configNode.SetValue("umbracoPageNotFound", configureNodes.Find(x => x.Name == "400").GetUdi().ToString());
            configNode.SetValue("siteCookiePolicyPage", configureNodes.Find(x => x.Name == "Cookie Policy").GetUdi().ToString());
            configNode.SetValue("siteSearchPage", configureNodes.Find(x => x.Name == "Search").GetUdi().ToString());

            contentService.Save(configNode);
        }

        private void InitLayoutNode(IContent layoutNode)
        {
            layoutNode.SetValue("headerExternalLinks", string.Join(",", headerExtLinks.Select(x => x.GetUdi().ToString())));

            contentService.Save(layoutNode);
        }

        private void InitTags(IContent tagRootNode)
        {
            var categoryNode = contentService.CreateContentWithIdentity("Category", tagRootNode, "tag");

            foreach(var listNode in listNodes)
            {
                var tagName = listNode.Parent().Name == "Nursery" ? "Nursery " + listNode.Name : listNode.Name;
                var tag = contentService.CreateContentWithIdentity(tagName, categoryNode, "tag");

                var listPage = listNodes.Find(x => x.Id == listNode.Id);
                listPage.SetValue("searchFiltersOr", tag.GetUdi().ToString());

                contentService.Save(listPage);
            }
        }

        // USER GROUP
        private void CreateUserGroupPermissions(IContent siteNode)
        {
            //Unbelieveable. This is the only documentation which explains the permission argument values which can be used
            //https://our.umbraco.org/forum/developers/api-questions/70369-settings-permission-on-content-with-entitypermissionset

            var userService = ApplicationContext.Current.Services.UserService;
            var mediaService = ApplicationContext.Current.Services.MediaService;

            var mediaFolder = mediaService.CreateMedia(siteNode.Name, -1, "Folder");
            mediaService.Save(mediaFolder);

            var userGroupAlias = $"schoolEditor{siteNode.Name}";

            if (userService.GetUserGroupByAlias(userGroupAlias) == null)
            {
                var userGroup = new UserGroup
                {
                    Alias = userGroupAlias,
                    Name = $"School Editor {siteNode.Name}",
                    StartContentId = siteNode.Id,
                    StartMediaId = mediaFolder.Id,
                    Permissions = new List<string>() { "Z", "F" }
                };
                userGroup.AddAllowedSection("content");

                userService.Save(userGroup);

                foreach (var page in schoolEditorContentPages)
                {
                    foreach (var permission in "SAODMCUZKF")
                    {
                        userService.AssignUserGroupPermission(userGroup.Id, permission, page.Id);
                    }
                }
            }
        }
    }
}
