﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace IBV.EDC.Lib.Domain
{
    public static class SchoolStylesheet
    {
        public static string Get(string stylesheetNodeId)
        {
            // We technically don't need to be storing the ID for the stylesheet but we still need the json
            // for the drop-down in the back-end to hook up to the associated css file.
            // We could use the .css filename istead of the id.

            string cssFileName = null;
            using (StreamReader json = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["SchoolStylesheetData"])))
            {
                List<Stylesheet> items = JsonConvert.DeserializeObject<List<Stylesheet>>(json.ReadToEnd());
                cssFileName = items.Where(x => x.id == stylesheetNodeId).Select(x => x.css).FirstOrDefault();
            }

            return cssFileName;
        }
    }

    public struct Stylesheet
    {
        public string id;
        public string name;
        public string css;
    }
}