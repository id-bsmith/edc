﻿using Examine;
using IBV.EDC.Lib.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Umbraco.Core;

namespace IBV.EDC.Lib.Domain
{
    public class EventsFeed
    {
        public static HttpResponseMessage CalanderJson(string pageID, string tagFilters)
        {
            List<Models.Event> calendarEvents = new List<Models.Event>();
            ExamineEventsSearch query = new ExamineEventsSearch();
            List<SearchResult> searchResults = query.GetSearchResults(Convert.ToInt32(pageID), tagFilters).ToList<SearchResult>();
            
            //loop the search results if we have any results
            if ((searchResults != null) && (searchResults.Count > 0))
            {
                foreach(var item in searchResults)
                {
                    if (item.Fields.ContainsKey("eventTitle"))
                    {
                        Models.EventExcludedDates ed = new Models.EventExcludedDates();
                        string eventFrequency = "";
                        int daysToAdd = 0;
                        List<DateTime> excludedDates = new List<DateTime>();
                        Models.Event evnt = new Models.Event();

                        evnt.start = item.Fields["eventStartDateTime"];
                        evnt.end = item.Fields["eventEndDateTime"];
                        DateTime startDate = DateTime.Parse(evnt.start);
                        DateTime endDate = DateTime.Parse(evnt.end);

                        //Deserialize the Archetype excluded dates if they exists and add to a custom list.
                        if (item.Fields.ContainsKey("eventExcludedDates"))
                        {
                            var settings = new Newtonsoft.Json.JsonSerializerSettings
                            { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore, MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore };
                            ed = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.EventExcludedDates>(item.Fields["eventExcludedDates"], settings);

                            if (ed.fieldsets[0].properties.Count() > 0)
                            {
                                //Add each excluded date in the Archetype property to a list.
                                foreach (var excludedDate in ed.fieldsets)
                                {
                                    excludedDates.Add(excludedDate.properties.First().value.Date);
                                }
                            }
                        }

                        //Get the event frequency if a recurring event.
                        if (item.Fields.ContainsKey("eventFrequency"))
                        {
                            eventFrequency = item.Fields["eventFrequency"].ToString();
                            //Get the days to skip based on the event frequency.
                            daysToAdd = GetDaysToSkip(eventFrequency);
                        }


                        if (startDate.Date == endDate.Date)
                        {
                            // If endDate is greater than now
                            if (DateTime.Compare(endDate, DateTime.Now) > 0)
                            {
                                //single (non-recurring) event.
                                //Retreive the url if it exists
                                if (item.Fields.ContainsKey("eventArticleLink"))
                                {
                                    evnt.url = GetUrlFromUDI(item.Fields["eventArticleLink"]);
                                }

                                //set the times for use as extra properties.
                                try
                                {
                                    evnt.EventStartTime = evnt.start.Substring(11, 5);
                                }
                                catch (Exception) { evnt.EventStartTime = "00:00"; }
                                try
                                {
                                    evnt.EventEndTime = evnt.end.Substring(11, 5);
                                }
                                catch (Exception) { evnt.end = "00:00"; }

                                evnt.title = item.Fields["eventTitle"];
                                evnt.EventDetails = item.Fields["eventDetails"];

                                if (item.Fields["eventHideTime"] == "1")
                                {
                                    evnt.allDay = true;
                                    //We need to send just the date (without the time as fullcalendar looks for this for it's logic)
                                    //If either the start or end value has a "T" as part of the ISO8601 date string, allDay will become false.
                                    evnt.start = item.Fields["eventStartDateTime"].Substring(0, 10);
                                    evnt.end = item.Fields["eventEndDateTime"].Substring(0, 10);
                                }
                                else
                                {
                                    evnt.allDay = false;
                                }
                                
                                calendarEvents.Add(evnt);
                            }
                        }
                        else if (endDate.Date > startDate.Date)
                        {
                            //Recurring event
                            DateTime nextDate = startDate;

                            //Add the events for each week in the range excluding any excluded events.
                            if ((eventFrequency == "Weekly") || (eventFrequency == "Fortnightly"))
                            {
                                for (DateTime date = nextDate.Date; date.Date <= endDate.Date; date = date.AddDays(daysToAdd))
                                {
                                    //only return events in the future
                                    if (date.Date >= DateTime.Now.Date)
                                    {
                                        //check if the current date is in the excluded list before adding to the calendar event list.
                                        if (!excludedDates.Contains(date.Date))
                                        {
                                            evnt = new Models.Event();
                                            evnt.title = item.Fields["eventTitle"];
                                            evnt.start = item.Fields["eventStartDateTime"];
                                            evnt.end = item.Fields["eventEndDateTime"];
                                            evnt.EventDetails = item.Fields["eventDetails"];
                                            
                                            //Retreive the url if it exists
                                            if (item.Fields.ContainsKey("eventArticleLink"))
                                            {
                                                evnt.url = GetUrlFromUDI(item.Fields["eventArticleLink"]);
                                            }

                                            //set the times for use as extra properties.
                                            try
                                            {
                                                evnt.EventStartTime = evnt.start.Substring(11, 5);
                                            }
                                            catch (Exception) { evnt.EventStartTime = "00:00"; }
                                            try
                                            {
                                                evnt.EventEndTime = evnt.end.Substring(11, 5);
                                            }
                                            catch (Exception) { evnt.end = "00:00"; }

                                            evnt.start = date.ToString("yyyy-MM-ddT") + evnt.EventStartTime;
                                            evnt.end = date.ToString("yyyy-MM-ddT") + evnt.EventEndTime;

                                            if (item.Fields["eventHideTime"] == "1")
                                            {
                                                evnt.allDay = true;
                                                //We need to send just the date (without the time as fullcalendar looks for this for it's logic)
                                                //If either the start or end value has a "T" as part of the ISO8601 date string, allDay will become false.
                                                evnt.start = date.ToString("yyyy-MM-dd");
                                                evnt.end = date.ToString("yyyy-MM-dd");
                                            }
                                            else
                                            {
                                                evnt.allDay = false;
                                            }
                                            calendarEvents.Add(evnt);
                                        }
                                    }
                                }
                            }

                            if (eventFrequency == "Monthly")
                            {
                                var firstDayOfMonth = new DateTime(nextDate.Year, nextDate.Month, 1);
                                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                                bool isLastDayOfMonth = false;

                                if (lastDayOfMonth.Day == nextDate.Day)
                                {
                                    isLastDayOfMonth = true;
                                }

                                for (DateTime date = nextDate.Date; date.Date <= endDate.Date; date = date.AddMonths(1))
                                {
                                    //only return events in the future
                                    if (date.Date >= DateTime.Now.Date)
                                    {
                                        //check if the current date is in the excluded list before adding to the calendar event list.
                                        if (!excludedDates.Contains(date.Date))
                                        {
                                            evnt = new Models.Event();
                                            evnt.title = item.Fields["eventTitle"];
                                            evnt.start = item.Fields["eventStartDateTime"];
                                            evnt.end = item.Fields["eventEndDateTime"];
                                            evnt.EventDetails = item.Fields["eventDetails"];
                                            
                                            //Retreive the url if it exists
                                            if (item.Fields.ContainsKey("eventArticleLink"))
                                            {
                                                evnt.url = GetUrlFromUDI(item.Fields["eventArticleLink"]);
                                            }

                                            //set the times for use as extra properties.
                                            try
                                            {
                                                evnt.EventStartTime = evnt.start.Substring(11, 5);
                                            }
                                            catch (Exception)
                                            { evnt.EventStartTime = "00:00"; }
                                            try
                                            {
                                                evnt.EventEndTime = evnt.end.Substring(11, 5);
                                            }
                                            catch (Exception)

                                            { evnt.end = "00:00"; }

                                            evnt.allDay = false;

                                            if (isLastDayOfMonth)
                                            {
                                                int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                                                evnt.start = date.ToString("yyyy-MM-" + daysInMonth + "T") + evnt.EventStartTime;
                                                evnt.end = date.ToString("yyyy-MM-" + daysInMonth + "T") + evnt.EventEndTime;
                                                if (item.Fields["eventHideTime"] == "1")
                                                {
                                                    evnt.allDay = true;
                                                    //We need to send just the date (without the time as fullcalendar looks for this for it's logic)
                                                    //If either the start or end value has a "T" as part of the ISO8601 date string, allDay will become false.
                                                    evnt.start = date.ToString("yyyy-MM-" + daysInMonth);
                                                    evnt.end = date.ToString("yyyy-MM-" + daysInMonth);
                                                }
                                            }
                                            else
                                            {
                                                evnt.start = date.ToString("yyyy-MM-ddT") + evnt.EventStartTime;
                                                evnt.end = date.ToString("yyyy-MM-ddT") + evnt.EventEndTime;
                                                if (item.Fields["eventHideTime"] == "1")
                                                {
                                                    evnt.allDay = true;
                                                    //We need to send just the date (without the time as fullcalendar looks for this for it's logic)
                                                    //If either the start or end value has a "T" as part of the ISO8601 date string, allDay will become false.
                                                    evnt.start = date.ToString("yyyy-MM-dd");
                                                    evnt.end = date.ToString("yyyy-MM-dd");
                                                }
                                            }

                                            calendarEvents.Add(evnt);
                                        }
                                    }
                                }
                            }

                            //This is a sort of hack to deal with dates spaning multiple days but they are not a recuring event.
                            //There is no event frequency and the end date is greater than the start date.
                            //We loop each day in the date range and add each event to the calendarEvents event list within the range.
                            if (eventFrequency == "")
                            {
                                for (DateTime date = nextDate.Date; date.Date <= endDate.Date; date = date.AddDays(1))
                                {
                                    //only return events in the future and check if the current date is in the excluded list before adding to the calendar event list
                                    if (date.Date >= DateTime.Now.Date && !excludedDates.Contains(date.Date))
                                    {
                                        evnt = new Models.Event();
                                        evnt.title = item.Fields["eventTitle"];
                                        evnt.start = item.Fields["eventStartDateTime"];
                                        evnt.end = item.Fields["eventEndDateTime"];
                                        evnt.EventDetails = item.Fields["eventDetails"];
                                        
                                        //Retreive the url if it exists
                                        if (item.Fields.ContainsKey("eventArticleLink"))
                                        {
                                            evnt.url = GetUrlFromUDI(item.Fields["eventArticleLink"]);
                                        }

                                        //set the times for use as extra properties.
                                        try
                                        {
                                            evnt.EventStartTime = evnt.start.Substring(11, 5);
                                        }
                                        catch (Exception) { evnt.EventStartTime = "00:00"; }
                                        try
                                        {
                                            evnt.EventEndTime = evnt.end.Substring(11, 5);
                                        }
                                        catch (Exception) { evnt.end = "00:00"; }

                                        evnt.start = date.ToString("yyyy-MM-ddT") + evnt.EventStartTime;
                                        evnt.end = date.ToString("yyyy-MM-ddT") + evnt.EventEndTime;

                                        if (item.Fields["eventHideTime"] == "1")
                                        {
                                            evnt.allDay = true;
                                            //We need to send just the date (without the time as fullcalendar looks for this for it's logic)
                                            //If either the start or end value has a "T" as part of the ISO8601 date string, allDay will become false.
                                            evnt.start = date.ToString("yyyy-MM-dd");
                                            evnt.end = date.ToString("yyyy-MM-dd");
                                        }
                                        else
                                        {
                                            evnt.allDay = false;
                                        }
                                        calendarEvents.Add(evnt);
                                    }
                                }
                            }

                        }

                    }
                }
            }

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(calendarEvents);
            return new HttpResponseMessage()
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };
        }


        public static string GetUrlFromUDI(string udiUrl)
        {
            if (Udi.TryParse(udiUrl, out Udi udi))
            {
                var umbracoHelper = IPublishedContentTools.GetUmbracoHelper();
                int urlID = umbracoHelper.GetIdForUdi(udi);
                return umbracoHelper.Url(urlID);
            }
            else
            {
                return "";
            }
        }


        /// <summary>
        /// Set the days for a recurring frequency
        /// </summary>
        /// <param name="Frequency"></param>
        /// <returns>int</returns>
        public static int GetDaysToSkip(string Frequency)
        {
            int days = 0;
            switch (Frequency)
            {
                case "Weekly":
                    days = 7;
                    break;
                case "Fortnightly":
                    days = 14;
                    break;
                case "4 Weekly":
                    days = 28;
                    break;
            }

            return days;
        }
    }
}
