﻿using IBV.EDC.Lib.Tools;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Domain
{
    public static class SiteAlert
    {
        public static bool Display(IPublishedContent siteAlertNode)
        {
            var showAlert = siteAlertNode.GetPropertyValue<bool>("siteAlertToggle");
            if (!showAlert)
            {
                return false;
            }

            // return false if cookie exists, expired AND out of date
            HttpCookie cookie = HttpContext.Current.Request.Cookies["SiteAlertCookie"];
            if (cookie?["Expires"] == "true" && cookie["updateDate"].Replace("%20", " ").Equals(siteAlertNode.UpdateDate.ToString()))
            {
                return false;
            }

            // return false if school isn't on the list
            var alertSchoolIds = siteAlertNode.GetPropertyValue<JArray>("siteAlertSchools").ToObject<List<int>>();
            if (alertSchoolIds == null || !alertSchoolIds.Contains(IPublishedContentTools.GetSiteNode().Id))
            {
                return false;
            }

            CreateCookie(cookie, siteAlertNode.UpdateDate);

            return true;
        }

        private static void CreateCookie(HttpCookie cookie, DateTime updateDate)
        {
            // create cookie
            if (cookie == null) cookie = new HttpCookie("SiteAlertCookie");
            cookie.Values["Expires"] = "false";
            cookie.Values["updateDate"] = updateDate.ToString();
            
            HttpContext.Current.Response.SetCookie(cookie);
        }
    }
}