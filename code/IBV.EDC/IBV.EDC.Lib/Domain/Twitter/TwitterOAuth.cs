﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace IBV.EDC.Lib.Domain.Twitter
{
    public class TwitterOAuth
    {
        public List<KeyValuePair<string, string>> Parameters = new List<KeyValuePair<string, string>>();
        public string FullUrl = null;
        public string EndPoint = null;
        public string HttpMethod = null;
        public string AuthorisationHeader = null;

        public TwitterOAuth(TwitterHttpMethod httpMethod, TwitterEndPoint endPoint, string queryParams = null)
        {
            HttpMethod = Enum.GetName(typeof(TwitterHttpMethod), httpMethod).ToUpper();
            EndPoint = GetTwitterEndPointURL(endPoint);
            FullUrl = $"{EndPoint}{queryParams}";

            Parameters.Add(new KeyValuePair<string, string>("oauth_consumer_key", ConfigurationManager.AppSettings["Twitter.ConsumerKey"]));
            Parameters.Add(new KeyValuePair<string, string>("oauth_nonce", Convert.ToBase64String(Guid.NewGuid().ToByteArray())));
            Parameters.Add(new KeyValuePair<string, string>("oauth_signature_method", "HMAC-SHA1"));
            Parameters.Add(new KeyValuePair<string, string>("oauth_timestamp", UnixTimeNowSeconds()));
            Parameters.Add(new KeyValuePair<string, string>("oauth_token", ConfigurationManager.AppSettings["Twitter.AccessToken"]));
            Parameters.Add(new KeyValuePair<string, string>("oauth_version", "1.0"));

            NameValueCollection queryParamsKvp = HttpUtility.ParseQueryString(queryParams);
            foreach (string key in queryParamsKvp.AllKeys)
            {
                Parameters.Add(new KeyValuePair<string, string>(key, queryParamsKvp[key]));
            }

            Parameters.Add(new KeyValuePair<string, string>("oauth_signature", GenerateSignature()));

            AuthorisationHeader = GetAuthorisationHeader();
        }

        private string GetAuthorisationHeader()
        {
            return "OAuth " + string.Join(", ", Parameters
                    .Where(kvp => kvp.Key.StartsWith("oauth_"))
                    .Select(kvp => $"{Uri.EscapeDataString(kvp.Key)}=\"{Uri.EscapeDataString(kvp.Value)}\"")
                    .OrderBy(s => s));
        }

        private string GenerateSignature()
        {
            var parameters = string.Join("&", Parameters.Union(Parameters).Select(kvp => $"{Uri.EscapeDataString(kvp.Key)}={Uri.EscapeDataString(kvp.Value)}").OrderBy(s => s));
            var fullSigData = $"{HttpMethod}&{Uri.EscapeDataString(EndPoint)}&{Uri.EscapeDataString(parameters)}";

            HMACSHA1 sigHasher = new HMACSHA1(new ASCIIEncoding().GetBytes($"{ConfigurationManager.AppSettings["Twitter.ConsumerSecret"]}&{ConfigurationManager.AppSettings["Twitter.AccessTokenSecret"]}"));

            return Convert.ToBase64String(sigHasher.ComputeHash(new ASCIIEncoding().GetBytes(fullSigData)));
        }

        private string GetTwitterEndPointURL(TwitterEndPoint twitterEndPoint)
        {
            switch (twitterEndPoint)
            {
                case TwitterEndPoint.UserTimeline:
                    return ConfigurationManager.AppSettings["Twitter.UserTimeline.URL"];
                default:
                    return null;
            }
        }

        private string UnixTimeNowSeconds()
        {
            return DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds.ToString();
        }
    }

    public enum TwitterHttpMethod
    {
        [Description("GET")]
        Get
    }

    public enum TwitterEndPoint
    {
        [Description("User Timeline")]
        UserTimeline = 1
    }
}