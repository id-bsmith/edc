﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace IBV.EDC.Lib.Domain.Twitter

{
    [DataContract]
    public class TwitterPost
    {
        private const string dateFormat = "ddd MMM dd HH:mm:ss zzz yyyy";

        [DataMember(Name = "id_str")]
        public string IdStr { get; set; }

        [IgnoreDataMember]
        public string Link
        {
            get
            {
                return string.Format("http://twitter.com/{0}/status/{1}", this.User.ScreenName, this.IdStr);
            }
        }

        [IgnoreDataMember]
        public long Id
        {
            get
            {
                return long.Parse(this.IdStr);
            }
        }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [IgnoreDataMember]
        public string DisplayText { get; set; }

        [DataMember(Name = "created_at")]
        public string CreatedAt { get; set; }

        [IgnoreDataMember]
        public DateTime DateCreated
        {
            get
            {
                return DateTime.ParseExact(CreatedAt, dateFormat, CultureInfo.InvariantCulture);
            }
        }

        [IgnoreDataMember]
        public TimeSpan Since
        {
            get
            {
                return DateTime.Now.Subtract(DateCreated);
            }
        }

        [IgnoreDataMember]
        public string SinceFormatted
        {
            get
            {

                if (this.Since.Days > 0)
                {
                    return this.DateCreated.ToString("dd MMM");
                }
                else if (this.Since.Hours > 0)
                {
                    return string.Format("{0}h ago", this.Since.Hours);
                }
                else if (this.Since.Minutes > 0)
                {
                    return string.Format("{0}m ago", this.Since.Minutes);
                }
                else if (this.Since.Seconds > 0)
                {
                    return string.Format("{0}s ago", this.Since.Seconds);
                }

                return this.DateCreated.ToString("dd MMM");
            }
        }

        [DataMember(Name = "user")]
        public TwitterUser User { get; set; }

        [DataMember(Name = "entities")]
        public TwitterEntities Entities { get; set; }
    }

    [DataContract]
    public class TwitterUser
    {
        [DataMember(Name = "url")]
        public string Link { get; set; }

        [DataMember(Name = "profile_image_url_https")]
        public string ProfileImageUrl { get; set; }

        [DataMember(Name = "screen_name")]
        public string ScreenName { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    public class TwitterEntities
    {
        [DataMember(Name = "media")]
        public List<TwitterEntity> Media { get; set; }

        [DataMember(Name = "urls")]
        public List<TwitterEntity> Urls { get; set; }

        [DataMember(Name = "user_mentions")]
        public List<TwitterEntity> UserMentions { get; set; }

        [DataMember(Name = "hashtags")]
        public List<TwitterEntity> Hashtags { get; set; }
    }

    [DataContract]
    public class TwitterEntity
    {
        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "media_url")]
        public string MediaUrl { get; set; }

        [DataMember(Name = "display_url")]
        public string DisplayUrl { get; set; }

        [DataMember(Name = "indices")]
        public int[] Indices { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "screen_name")]
        public string ScreenName { get; set; }

        [IgnoreDataMember]
        // These should not really be here since they deal with UI logic but it is preferable to parse the text before adding to cache
        // rather than with every page load
        public string Link
        {
            get
            {
                // hashtag
                if (!string.IsNullOrEmpty(this.Text))
                {
                    return string.Format("<a href=\"http://search.twitter.com/?tag={0}\">#{0}</a>", this.Text);
                }
                // user mention
                if (!string.IsNullOrEmpty(this.ScreenName))
                {
                    return string.Format("<a href=\"http://www.twitter.com/{0}\">@{0}</a>", this.ScreenName);
                }
                // picture
                if (!string.IsNullOrEmpty(this.MediaUrl))
                {
                    return string.Format("<a href=\"{0}\">{1}</a>", this.MediaUrl, this.DisplayUrl);
                }
                // link
                if (!string.IsNullOrEmpty(this.Url))
                {
                    return string.Format("<a href=\"{0}\">{1}</a>", this.Url, this.DisplayUrl);
                }

                // unsure return nothing
                return string.Empty;
            }
        }
    }
}