﻿using System.IO;
using System.Net;

namespace IBV.EDC.Lib.Domain.Twitter
{
    public static class TwitterResponse
    {
        public static string GetResponseAsResult(TwitterOAuth twitterOAuth)
        {
            WebRequest request = WebRequest.Create(twitterOAuth.FullUrl);
            request.Method = twitterOAuth.HttpMethod;
            request.Headers.Add("Authorization", twitterOAuth.AuthorisationHeader);

            StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream());

            return reader.ReadToEnd();
        }
    }
}