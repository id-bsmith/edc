﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBV.EDC.Lib.Domain.Twitter
{
    public static class TwitterApi
    {
        public static List<TwitterPost> GetUserTimeline(string screenName, int tweetCount, bool includeRetweets, bool excludeReplies)
        {
            try
            {
                string queryParams = $"?screen_name={screenName}&count={tweetCount}&exclude_replies={excludeReplies}&include_rts={includeRetweets}";
                string result = TwitterResponse.GetResponseAsResult(new TwitterOAuth(TwitterHttpMethod.Get, TwitterEndPoint.UserTimeline, queryParams));
                List<TwitterPost> tweets = JsonConvert.DeserializeObject<List<TwitterPost>>(result);

                for (int i = 0; i < tweets.Count; i++)
                {
                   tweets[i] = ProcessTweet(tweets[i]);
                }

                return tweets;
            }
            catch (Exception)
            {
                //return nothing if error happens
                return new List<TwitterPost>();
            }
        }

        private static TwitterPost ProcessTweet(TwitterPost tweet)
        {
            // Cannot do a simple replacement becuase the idices would then be wrong
            // instead we bunch them all together then order by start index so we can step through
            // and rebuild the string
            List<TwitterEntity> allEntities = new List<TwitterEntity>();
            if (tweet.Entities.Hashtags != null)
                allEntities.AddRange(tweet.Entities.Hashtags);
            if (tweet.Entities.Media != null)
                allEntities.AddRange(tweet.Entities.Media);
            if (tweet.Entities.Urls != null)
                allEntities.AddRange(tweet.Entities.Urls);
            if (tweet.Entities.UserMentions != null)
                allEntities.AddRange(tweet.Entities.UserMentions);

            StringBuilder sbProcessedTweet = new StringBuilder();
            int idx = 0;

            foreach (TwitterEntity entity in allEntities.OrderBy(e => e.Indices[0]))
            {
                int len = entity.Indices[0] - idx;
                if (len > 0)
                {
                    sbProcessedTweet.Append(tweet.Text.Substring(idx, len));
                    sbProcessedTweet.Append(entity.Link);
                    idx = entity.Indices[1];
                }
            }

            int lastIdx = tweet.Text.Length;

            if (idx < lastIdx)
            {
                int len = lastIdx - idx;
                sbProcessedTweet.Append(tweet.Text.Substring(idx, len));
            }

            tweet.DisplayText = sbProcessedTweet.ToString();

            return tweet;
        }
    }
}