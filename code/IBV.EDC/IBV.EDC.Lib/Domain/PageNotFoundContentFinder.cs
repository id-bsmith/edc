﻿using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace IBV.EDC.Lib.Domain
{
    class PageNotFoundContentFinder : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            var domain = contentRequest?.UmbracoDomain;
            if (domain != null)
            {
                var homeNode = domain.RootContentId.HasValue && domain.RootContentId.Value > 0 ?
                                contentRequest.RoutingContext.UmbracoContext.ContentCache.GetById(domain.RootContentId.Value) : null;

                var configurationNode = homeNode?.Siblings().Where(x => x.DocumentTypeAlias == "settings").FirstOrDefault()
                                                 .Children().Where(x => x.DocumentTypeAlias == "configuration").FirstOrDefault();
                if (configurationNode?.Id > 0)
                {
                    var pageNotFound = configurationNode.GetPropertyValue<IPublishedContent>("umbracoPageNotFound");
                    if (pageNotFound?.Id > 0)
                    {
                        contentRequest.PublishedContent = pageNotFound;
                        contentRequest.SetIs404();
                    }
                }
            }

            return contentRequest.PublishedContent != null;
        }
    }
}
