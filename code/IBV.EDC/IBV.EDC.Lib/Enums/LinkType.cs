﻿using System;
using System.ComponentModel;

namespace IBV.EDC.Lib.Enums
{
    [Flags]
    public enum LinkType
    {
        [Description("Internal Link")]
        InternalLink = 1,

        [Description("External Link")]
        ExternalLink = 2,

        [Description("Media Link")]
        MediaLink = 3
    }
}