﻿using System.ComponentModel;

namespace IBV.EDC.Lib.Enums
{
    public enum SearchPeriod
    {
        [Description("All")]
        All = 1,

        [Description("Past")]
        Past = 2,

        [Description("Future")]
        Future = 3
    }
}
