﻿using System.ComponentModel;

namespace IBV.EDC.Lib.Enums
{
    public enum ImageCropOption
    {
        // Note: The description attribute is used to store the aspect ratio of the image.
        // This can be used to calculate the crop height when a resized version of the crop is requested.

        [Description("0.0")]
        None = 1,

        [Description("3.2")]
        Banner = 2,

        [Description("1.5")]
        Landscape = 3,

        [Description("0.6666666667")]
        Portrait = 4,

        [Description("1.0")]
        Square = 5,

        [Description("2.0")]
        Gallery = 6
    }
}
