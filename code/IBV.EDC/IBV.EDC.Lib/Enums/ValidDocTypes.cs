﻿using nuPickers.Shared.EnumDataSource;
using System.ComponentModel;


namespace IBV.EDC.Lib.Enums
{
    public enum ValidDocTypes
    {
        //[Description("None")]
        //None = 1,

        [Description("Calendar")]
        Calendar = 2,

        [Description("ContactForm")]
        [EnumDataSource(Label = "Contact Form")]
        ContactForm = 3,

        [Description("Error404")]
        [EnumDataSource(Label = "Error Form")]
        ErrorForm = 4,

        [Description("EventArticle")]
        [EnumDataSource(Label = "Event Article")]
        EventArticle = 5,

        [Description("FrequentlyAskedQuestions")]
        [EnumDataSource(Label = "FAQs")]
        FAQs = 5,

        [Description("Gateway")]
        Gateway = 6,

        [Description("GeneralArticle")]
        [EnumDataSource(Label = "General Article")]
        GeneralArticle = 7,

        [Description("GeneralArticleAlternative")]
        [EnumDataSource(Label = "General Article Alternative")]
        GeneralArticleAlternative = 8,

        [Description("Home")]
        Home = 9,

        [Description("ListDocument")]
        [EnumDataSource(Label = "List Document")]
        ListDocument = 10,

        [Description("List Event")]
        [EnumDataSource(Label = "List Event")]
        ListEvent = 11,

        [Description("ListNews")]
        [EnumDataSource(Label = "List News")]
        ListNews = 12,

        [Description("ListSearch")]
        [EnumDataSource(Label = "List Search")]
        ListSearch = 13,

        [Description("ListUsefulLinks")]
        [EnumDataSource(Label = "List Useful Links")]
        ListUsefulLinks = 14,

        [Description("NewsArticle")]
        [EnumDataSource(Label = "News Article")]
        NewsArticle = 15
    }
}
