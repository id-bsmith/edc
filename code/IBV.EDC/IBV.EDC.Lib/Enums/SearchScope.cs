﻿using System.ComponentModel;

namespace IBV.EDC.Lib.Enums
{
    public enum SearchScope
    {
        [Description("Descendants")]
        Descendants = 1,

        [Description("Children")]
        Children = 2,

        [Description("Site")]
        Site = 3,
    }
}
