﻿using System.ComponentModel;

namespace IBV.EDC.Lib.Enums
{
    public enum SearchOrder
    {
        [Description("None")]
        None = 1,

        [Description("Latest")]
        Latest = 2,

        [Description("Oldest")]
        Oldest = 3,

        [Description("Alphabetical")]
        Alphabetical = 4,

        [Description("Relevancy")]
        Relevancy = 5
    }
}
