﻿using System.ComponentModel;
using nuPickers.Shared.EnumDataSource;

namespace IBV.EDC.Lib.Enums
{
    public enum SearchType
    {
        [Description("Content and Pdfs")]
        [EnumDataSource(Label="Content and Pdfs")]
        ContentAndPdfs = 1,

        [Description("Content")]
        Content = 2,

        [Description("Pdfs")]
        Pdfs = 3,

        [Description("Useful Links")]
        [EnumDataSource(Label = "Useful Links")]
        UsefulLinks = 4,
        
        [Description("Events")]
        Events = 5
    }
}