﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBV.EDC.Lib.Models
{
    public class Event
    {
        public string title { get; set; }
        public string start { get; set; }
        public string EventStartTime { get; set; }
        public string end { get; set; }
        public string EventEndTime { get; set; }
        public string EventDetails { get; set; }
        public string url { get; set; }
        public string EventExcludedDates { get; set; }
        public bool allDay { get; set; }
    }

    public class UpComingEvents
    {
        public List<Event> Events { get; set; }
        public string Heading { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
        public int ResultCount { get; set; }

        public UpComingEvents()
        {
            this.Events = new List<Event>();
        }
    }


    /// <summary>
    /// The following classes are used by the excluded dates Arctype property which returns 1 or more dates.
    /// </summary>
    public class Property
    {

        [JsonProperty("alias")]
        public string alias { get; set; }

        [JsonProperty("value")]
        public DateTime value { get; set; }
    }

    public class Fieldset
    {

        [JsonProperty("properties")]
        public IList<Property> properties { get; set; }

        [JsonProperty("alias")]
        public string alias { get; set; }

        [JsonProperty("disabled")]
        public bool disabled { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("releaseDate")]
        public object releaseDate { get; set; }

        [JsonProperty("expireDate")]
        public object expireDate { get; set; }

        [JsonProperty("allowedMemberGroups")]
        public string allowedMemberGroups { get; set; }
    }

    public class EventExcludedDates
    {
        [JsonProperty("fieldsets")]
        public IList<Fieldset> fieldsets { get; set; }
    }

}
