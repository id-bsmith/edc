﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class LinkList : ILinkList
    {
        // PROPERTIES

        public bool IsValid => (Links.Count > 0);
        public IList<ILink> Links { get; private set; }


        // FACTORY

        public static ILinkList Get(IEnumerable<IPublishedContent> contentList)
        {
            return new LinkList(contentList);
        }


        // CTORS

        private LinkList(IEnumerable<IPublishedContent> contentList)
        {
            Links = new List<ILink>();

            LoadData(contentList);
        }


        // METHODS

        private void LoadData(IEnumerable<IPublishedContent> contentList)
        {
            if(contentList?.Count() > 0)
            {
                foreach(var content in contentList)
                {
                    ILink link = Link.Get(content);
                    if(link.IsValid)
                    {
                        Links.Add(link);
                    }
                }
            }
        }
    }
}
