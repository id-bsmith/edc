﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Models.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class MenuLink : IMenuLink
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public string Url { get; private set; }
        public string Title { get; private set; }
        public IImage Image { get; private set; }


        // FACTORY

        public static IMenuLink Get(IPublishedContent content)
        {
            string cacheKey = string.Format("MenuLink-{0}", content.Id);

            IMenuLink menuLink = CacheHelper.Get(cacheKey) as MenuLink;
            if (menuLink != null)
            {
                return menuLink;
            }

            menuLink = new MenuLink(content);
            CacheHelper.Add(cacheKey, menuLink);
            return menuLink;
        }


        // CTORS

        private MenuLink(IPublishedContent content)
        {
            LoadNodeData(content);
        }


        // METHODS

        private void LoadNodeData(IPublishedContent content)
        {
            if (content.Id > 0)
            {
                IsValid = true;
                Id = content.Id;
                Url = content.Url;
                Title = !string.IsNullOrEmpty(content.GetPropertyValue<string>("nodeLinkTitle")) ? content.GetPropertyValue<string>("nodeLinkTitle") : content.Name;
                Image = Models.Image.Get(content.GetPropertyValue<IPublishedContent>("menuImage"));
            }
        }
    }
}