﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class Menu : IMenu
    {
        private const int MAIN_MENU_END_LEVEL = 4;
        private const int SIDE_MENU_START_LEVEL = 3;
        private const int SIDE_MENU_END_LEVEL = 4;

        // PROPERTIES

        public bool IsValid { get; private set; }
        public IMenuLink MenuLink { get; private set; }
        public IList<IMenu> SubMenus { get; private set; }


        // FACTORY

        public static IMenu Get(IPublishedContent rootNode, int endLevel = MAIN_MENU_END_LEVEL, bool ignoreRootHiddenStatus = false, bool isSideMenu = false)
        {
            int rootNodeId = rootNode != null ? rootNode.Id : 0;
            string cacheKey = string.Format("Menu-{0}-{1}-{2}", rootNodeId, endLevel, ignoreRootHiddenStatus.ToString());

            IMenu menu = CacheHelper.Get(cacheKey) as Menu;
            if (menu != null)
            {
                return menu;
            }

            menu = new Menu(rootNode, endLevel, ignoreRootHiddenStatus, isSideMenu);
            CacheHelper.Add(cacheKey, menu);
            return menu;
        }

        public static IMenu Get(int startLevel = SIDE_MENU_START_LEVEL, int endLevel = SIDE_MENU_END_LEVEL, bool ignoreRootHiddenStatus = false, bool isSideMenu = false)
        {
            return Get(IPublishedContentTools.GetAncestorByLevel(IPublishedContentTools.GetCurrentNode(), startLevel), endLevel, ignoreRootHiddenStatus, isSideMenu);
        }


        // CTORS

        private Menu(IPublishedContent rootNode, int endLevel, bool ignoreRootHiddenStatus, bool isSideMenu)
        {
            SubMenus = new List<IMenu>();
            if (!isSideMenu)
            {
                LoadData(rootNode, endLevel, ignoreRootHiddenStatus, false);
            }
            else
            {
                LoadSideData(rootNode, endLevel, ignoreRootHiddenStatus, true);
            }
        }


        // METHODS

        private void LoadData(IPublishedContent rootNode, int endLevel, bool ignoreRootHiddenStatus, bool isSideMenu)
        {
            if (rootNode.Id > 0 && umbraco.library.HasAccess(rootNode.Id, rootNode.Path))
            {
                MenuLink = Models.MenuLink.Get(rootNode);
                if (MenuLink.IsValid)
                {
                    IsValid = true;

                    if (ignoreRootHiddenStatus || rootNode.GetPropertyValue<bool>("umbracoNaviHide") != true)
                    {
                        // Load submenus
                        if (rootNode.Level <= endLevel)
                        {
                            foreach (var childNode in rootNode.Children<IPublishedContent>().Where(x => x.GetPropertyValue<bool>("umbracoNaviHide") == false))
                            {
                                IMenu subMenu = Get(childNode, endLevel, ignoreRootHiddenStatus, false);
                                if (subMenu.IsValid)
                                {
                                    SubMenus.Add(subMenu);
                                }
                            }
                        }
                    }
                }
            }
        }


        private void LoadSideData(IPublishedContent rootNode, int endLevel, bool ignoreRootHiddenStatus, bool isSideMenu)
        {
            if (rootNode != null && rootNode.Id > 0 && umbraco.library.HasAccess(rootNode.Id, rootNode.Path))
            {
                MenuLink = Models.MenuLink.Get(rootNode);
                if (MenuLink.IsValid)
                {
                    IsValid = true;

                    if (ignoreRootHiddenStatus || rootNode.GetPropertyValue<bool>("hideFromSideMenu") != true)
                    {
                        // Load submenus
                        if (rootNode.Level <= endLevel)
                        {
                            foreach (var childNode in rootNode.Children<IPublishedContent>().Where(x => x.GetPropertyValue<bool>("hideFromSideMenu") == false))
                            {
                                IMenu subMenu = Get(childNode, endLevel, ignoreRootHiddenStatus, true);
                                if (subMenu.IsValid)
                                {
                                    SubMenus.Add(subMenu);
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}
