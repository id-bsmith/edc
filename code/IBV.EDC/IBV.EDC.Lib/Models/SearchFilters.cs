﻿using IBV.EDC.Lib.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class SearchFilters : ISearchFilters
    {
        // PROPERTIES

        public bool IsValid => (ShowKeywordFilter || ShowDateFilter || ShowFilterList);
        public int Id { get; private set; }
        public bool ShowKeywordFilter { get; private set; }
        public bool ShowDateFilter { get; private set; }
        public bool ShowFilterList => FilterList.Filters.Count > 0;
        public IList<string> Keywords { get; private set; }
        public DateTime From { get; private set; }
        public DateTime To { get; private set; }
        public IFilterList FilterList { get; private set; }
        public bool ShowAllResults { get; private set; }
        public int Page { get; private set; }


        // FACTORY

        public static ISearchFilters Get(IPublishedContent content)
        {
            return new SearchFilters(content);
        }


        // CTORS

        private SearchFilters(IPublishedContent content)
        {
            LoadDefaults();
            LoadData(content);
        }


        // METHODS

        private void LoadDefaults()
        {
            Id = 0;
            ShowKeywordFilter = false;
            ShowDateFilter = false;
            Keywords = new List<string>();
            From = DateTime.MinValue;
            To = DateTime.MaxValue;
            FilterList = Models.FilterList.Get(Enumerable.Empty<IPublishedContent>());
            ShowAllResults = false;
            Page = 1;
        }

        private void LoadData(IPublishedContent content)
        {
            if (content?.Id > 0)
            {
                Id = content.Id;

                // Load keywords
                ShowKeywordFilter = content.GetPropertyValue<bool>("showUserKeywordFilter") == true;
                if (ShowKeywordFilter)
                {
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["keywords"]))
                    {
                        Keywords = HttpContext.Current.Request.QueryString["keywords"].Trim().Split(' ').ToList();
                    }
                }

                // Load dates
                ShowDateFilter = content.GetPropertyValue<bool>("showUserDateFilter") == true;
                if (ShowDateFilter)
                {
                    // Load date from
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["from"]) && DateTime.TryParse(HttpContext.Current.Request.QueryString["from"].Trim(), out DateTime from))
                    {
                        From = from;
                    }

                    // Load date to
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["to"]) && DateTime.TryParse(HttpContext.Current.Request.QueryString["to"].Trim(), out DateTime to))
                    {
                        To = to;
                    }
                }

                // Load selected user filters
                if (content.GetProperty("userFilters") != null)
                {
                    FilterList = Models.FilterList.Get(content.GetPropertyValue<IList<IPublishedContent>>("userFilters"));

                    IEnumerable<string> filterParameters = HttpContext.Current.Request.Form.AllKeys.Where(x => x.StartsWith("filter-"));
                    foreach (string filterParameter in filterParameters)
                    {
                        string[] filterParts = filterParameter.Split('-');
                        if (filterParts.Length == 3)
                        {
                            if (int.TryParse(filterParts[2], out int filterParameterId))
                            {
                                IFilter filter = FilterList.Filters.Where(x => x.Id == filterParameterId && x.Name.ToLower() == filterParts[1]).First();
                                if (filter != null)
                                {
                                    string[] filterTagParameterIds = HttpContext.Current.Request.Form[filterParameter].Split(',');
                                    for (int i=0; i < filter.FilterTags.Count(); i++)
                                    {
                                        filter.FilterTags[i].IsSelected = filterTagParameterIds.Contains(filter.FilterTags[i].Tag.Id.ToString());
                                    }
                                }
                            }
                        }
                    }
                }

                // Load page
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["page"]))
                {
                    if (int.TryParse(HttpContext.Current.Request.QueryString["page"], out int page))
                    {
                        Page = page;
                    }
                    else
                    {
                        ShowAllResults = true;
                    }
                }
            }
        }
    }
}