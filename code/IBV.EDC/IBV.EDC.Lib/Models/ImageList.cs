﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class ImageList : IImageList
    {
        // VARIABLES

        public IPublishedContent _media;


        // PROPERTIES

        public bool IsValid => Images.Count > 0;
        public IList<IImage> Images { get; set; }

        // FACTORY

        public static IImageList Get(IEnumerable<IPublishedContent> mediaList)
        {
            return new ImageList(mediaList);
        }


        // CTORS

        private ImageList(IEnumerable<IPublishedContent> mediaList)
        {
            Images = new List<IImage>();
            LoadData(mediaList);
        }

        // METHODS

        private void LoadData(IEnumerable<IPublishedContent> mediaList)
        {
            if (mediaList?.Count() > 0)
            {
                foreach(var media in mediaList)
                {
                    IImage image = Image.Get(media);
                    if(image.IsValid)
                    {
                        Images.Add(image);
                    }
                }
            }
        }
    }
}
