﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Extensions;
using IBV.EDC.Lib.Models.Interfaces;
using System;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class Image : IImage
    {
        // VARIABLES

        public IPublishedContent _media;


        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public string Url { get; private set; }
        public string AltText { get; private set; }
        public string Summary { get; private set; }


        // FACTORY

        public static IImage Get(IPublishedContent media)
        {
            string cacheKey = string.Format("Image-{0}", media?.Id);

            IImage image = CacheHelper.Get(cacheKey) as Image;
            if (image != null)
            {
                return image;
            }

            image = new Image(media);
            CacheHelper.Add(cacheKey, image);
            return image;
        }


        // CTORS

        private Image(IPublishedContent media)
        {
            LoadData(media);
        }

        // METHODS

        private void LoadData(IPublishedContent media)
        {
            if (media?.Id > 0 && media.HasProperty("umbracoBytes"))
            {
                IsValid = true;
                Id = media.Id;
                Url = media.Url;
                AltText = media.GetPropertyValue<string>("altText");
                Summary = media.GetPropertyValue<string>("nodeSummary");

                _media = media;
            }
        }

        public string GetCropUrl(IImageCrop imageCrop)
        {
            // If no crop specified, return the full-sized image URL
            if (imageCrop.ImageCropOption.Equals(Enums.ImageCropOption.None))
            {
                return Url;
            }

            // If specific width used, return a re-sized version of the crop
            if (imageCrop.Width > 0)
            {
                if (double.TryParse(imageCrop.ImageCropOption.GetDescriptionAttribute(), out double aspectRatio) && aspectRatio > 0)
                {
                    return _media.GetCropUrl(
                        propertyAlias: "umbracoFile",
                        cropAlias: imageCrop.ImageCropOption.ToString(),
                        width: imageCrop.Width,
                        height: (int?)Math.Round(imageCrop.Width / aspectRatio),
                        preferFocalPoint: false,
                        furtherOptions: "&bgcolor=fff" //remove black padding
                    );
                }
            }

            // ... else return the full-sized crop
            return _media.GetCropUrl(
                propertyAlias: "umbracoFile",
                cropAlias: imageCrop.ImageCropOption.ToString()
            );
        }
    }
}
