﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class FaqList : IFaqList
    {
        // PROPERTIES

        public bool IsValid => Faqs.Count > 0;
        public IList<IFaq> Faqs { get; private set; }


        // FACTORY

        public static FaqList Get(IEnumerable<IPublishedContent> contentList)
        {
            return new FaqList(contentList);
        }


        // CTORS

        private FaqList(IEnumerable<IPublishedContent> contentList)
        {
            Faqs = new List<IFaq>();
            LoadData(contentList);
        }


        // METHODS

        private void LoadData(IEnumerable<IPublishedContent> contentList)
        {
            if (contentList.Count() > 0)
            {
                foreach (var content in contentList)
                {
                    IFaq faq = Faq.Get(content);
                    if (faq.IsValid)
                    {
                        Faqs.Add(faq);
                    }
                }
            }
        }
    }
}
