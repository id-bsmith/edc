﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;

namespace IBV.EDC.Lib.Models
{
    public class SearchResult : ISearchResult
    {
        // PROPERTIES

        public int Id { get; set; }
        public string Heading { get; set; }
        public string Author { get; set; }
        public IDate Date { get; set; }
        public IImage Image { get; set; }
        public string Summary { get; set; }
        public ITagList TagList { get; set; }
        public ILink Link { get; set; }
        public IDictionary<string, string> Properties { get; set; }


        // CTORS

        public SearchResult()
        {
            Properties = new Dictionary<string, string>();
        }
    }
}