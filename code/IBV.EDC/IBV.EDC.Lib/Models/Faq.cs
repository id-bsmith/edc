﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Models.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class Faq : IFaq
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public string Question { get; private set; }
        public string Answer { get; private set; }


        // FACTORY

        public static IFaq Get(IPublishedContent content)
        {
            string cacheKey = string.Format("Faq-{0}", content.Id);

            IFaq faq = CacheHelper.Get(cacheKey) as Faq;
            if (faq != null)
            {
                return faq;
            }

            faq = new Faq(content);
            CacheHelper.Add(cacheKey, faq);
            return faq;
        }


        // CTORS

        private Faq(IPublishedContent content)
        {
            LoadData(content);
        }


        // METHODS

        private void LoadData(IPublishedContent content)
        {
            if (content.Id > 0)
            {
                IsValid = true;
                Id = content.Id;
                Question = content.GetPropertyValue<string>("faqQuestion");
                Answer = content.GetPropertyValue<string>("faqAnswer");
            }
        }
    }
}
