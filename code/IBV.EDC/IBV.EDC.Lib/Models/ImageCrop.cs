﻿using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Enums;
using System;

namespace IBV.EDC.Lib.Models
{
    public class ImageCrop : IImageCrop
    {
        // PROPERTIES

        public ImageCropOption ImageCropOption { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }


        // CTORS

        public ImageCrop(ImageCropOption imageCropOption)
        {
            ImageCropOption = imageCropOption;
            Width = 0;
            Height = 0;
        }

        public ImageCrop(ImageCropOption imageCropOption, int width)
        {
            if (width <= 0)
            {
                throw new ArgumentException();
            }

            ImageCropOption = imageCropOption;
            Width = width;
            Height = 0;
        }
    }
}