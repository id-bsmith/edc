﻿using IBV.EDC.Lib.Models.Interfaces;
using System;

namespace IBV.EDC.Lib.Models
{
    public class Date : IDate
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public DateTime DateTime { get; private set; }


        // FACTORY

        public static IDate Get(object date)
        {
            return new Date(date);
        }


        // CTORS

        private Date(object date)
        {
            if (date != null)
            {
                LoadData(date);
            }
        }


        // METHODS

        private void LoadData(object date)
        {
            if (DateTime.TryParse(date.ToString(), out DateTime dateTime) && dateTime > DateTime.MinValue)
            {
                IsValid = true;
                DateTime = dateTime;
            }
        }
    }
}