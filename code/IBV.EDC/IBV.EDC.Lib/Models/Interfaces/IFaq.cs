﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFaq
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Question { get; }
        string Answer { get; }
    }
}
