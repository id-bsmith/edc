﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFlag
    {
        // PROPERTIES

        bool IsTrue { get; }
    }
}
