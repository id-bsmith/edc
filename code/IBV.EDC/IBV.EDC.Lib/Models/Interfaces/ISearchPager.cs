﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ISearchPager
    {
        // PROPERTIES

        bool IsValid { get; }
        bool PreviousPageIsValid { get; }
        int PreviousPageNumber { get; }
        bool NextPageIsValid { get; }
        int NextPageNumber { get; }
        IList<ISearchPagerLink> PagerLinks { get; }
    }
}
