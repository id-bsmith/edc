﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ISearchResultList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<ISearchResult> SearchResults { get; }
        string NoResultsText { get; }
        int PageCount { get; }
        int ResultCount { get; }
    }
}