﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IImage
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Url { get; }
        string AltText { get; }
        string Summary { get; }


        // METHODS

        string GetCropUrl(IImageCrop imageCrop);
    }
}
