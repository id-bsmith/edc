﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IImageList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<IImage> Images { get; }
    }
}
