﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ISearchResult
    {
        // PROPERTIES

        int Id { get; set; }
        string Heading { get; set; }
        string Author { get; set; }
        IDate Date { get; set; }
        IImage Image { get; set; }
        string Summary { get; set; }
        ITagList TagList { get; set; }
        ILink Link { get; set; }
        IDictionary<string, string> Properties { get; set; }
    }
}