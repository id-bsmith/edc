﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFilter
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Name { get; }
        IList<IFilterTag> FilterTags { get; set; }
    }
}
