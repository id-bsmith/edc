﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ITagList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<ITag> Tags { get; }
    }
}
