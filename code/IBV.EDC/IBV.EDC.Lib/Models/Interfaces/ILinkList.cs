﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ILinkList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<ILink> Links { get; }
    }
}
