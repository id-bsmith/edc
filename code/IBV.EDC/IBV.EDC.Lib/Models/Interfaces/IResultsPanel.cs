﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IResultsPanel
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Heading { get; }
        ILink Link { get; }
        string LinkText { get; }
    }
}