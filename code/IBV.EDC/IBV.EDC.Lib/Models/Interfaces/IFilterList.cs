﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFilterList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<IFilter> Filters { get; }
    }
}
