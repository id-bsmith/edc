﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFeatureList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<IFeature> Features { get; }
    }
}