﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IMenu
    {
        // PROPERTIES

        bool IsValid { get; }
        IMenuLink MenuLink { get; }
        IList<IMenu> SubMenus { get; }
    }
}
