﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFaqGroupList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<IFaqGroup> FaqGroups { get; }
    }
}
