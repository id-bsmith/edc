﻿using System;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ITag
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        Guid Guid { get; }
        int ParentId { get; }
        string Name { get; }
    }
}
