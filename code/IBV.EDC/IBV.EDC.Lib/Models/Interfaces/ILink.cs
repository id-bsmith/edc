﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ILink
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        Enums.LinkType Type { get; }
        string Url { get; }
        string Title { get; }
        IDate Date { get; }
        IImage Image { get; }
        string Summary { get; }
        IDictionary<string, string> Properties { get; }
    }
}
