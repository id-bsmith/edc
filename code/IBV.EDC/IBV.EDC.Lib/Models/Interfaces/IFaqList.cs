﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFaqList
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<IFaq> Faqs { get; }
    }
}