﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFeature
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Heading { get; }
        IImage Image { get; }
        ILink Link { get; }
    }
}