﻿using IBV.EDC.Lib.Enums;
namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IImageCrop
    {
        // PROPERTIES

        ImageCropOption ImageCropOption { get; }
        int Width { get; }
        int Height { get; }
    }
}