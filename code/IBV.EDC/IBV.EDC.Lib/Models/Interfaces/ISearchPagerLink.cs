﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ISearchPagerLink
    {
        // PROPERTIES

        int PageNumber { get; set; }
        bool IsCurrent { get; set; }
    }
}
