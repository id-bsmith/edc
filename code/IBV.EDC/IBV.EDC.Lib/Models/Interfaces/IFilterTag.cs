﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFilterTag
    {
        // PROPERTIES

        ITag Tag { get; set; }
        bool IsSelected { get; set; }
    }
}
