﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IFaqGroup
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Heading { get; }
        IList<IFaq> Faqs { get; }
    }
}
