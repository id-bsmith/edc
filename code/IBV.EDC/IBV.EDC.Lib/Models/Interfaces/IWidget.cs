﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IWidget
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Code { get; }
    }
}