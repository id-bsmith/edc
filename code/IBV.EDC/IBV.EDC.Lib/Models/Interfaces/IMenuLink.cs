﻿namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IMenuLink
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        string Url { get; }
        string Title { get; }
        IImage Image { get; }
    }
}
