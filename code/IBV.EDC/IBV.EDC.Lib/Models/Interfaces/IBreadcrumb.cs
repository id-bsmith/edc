﻿using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IBreadcrumb
    {
        // PROPERTIES

        bool IsValid { get; }
        IList<ILink> Links { get; }
    }
}
