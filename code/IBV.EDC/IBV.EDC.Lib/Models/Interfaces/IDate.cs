﻿using System;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface IDate
    {
        // PROPERTIES

        bool IsValid { get; }
        DateTime DateTime { get; }
    }
}
