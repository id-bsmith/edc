﻿using IBV.EDC.Lib.Enums;
using System;
using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ISearchSettings
    {
        // PROPERTIES

        bool IsValid { get; }
        int NodeId { get; }
        int SiteNodeId { get; }
        SearchType Type { get; }
        SearchScope Scope { get; }
        IList<Guid> FiltersAnd { get; }
        IList<Guid> FiltersOr { get; }
        SearchPeriod Period { get; }
        SearchOrder Order { get; }
        int PageSize { get; set; }
        string NoResultsText { get; }
        float Fuzziness { get; }
        int MaxResults { get; }
        IList<string> ContentSearchFields { get; }
        IList<string> PdfSearchFields { get; }
        IList<string> StopWords { get; set; }
        IList<string> ValidNodeTypes { get; set; }
        IList<int> ValidUserFilters { get; set;  }
    }
}