﻿using System;
using System.Collections.Generic;

namespace IBV.EDC.Lib.Models.Interfaces
{
    public interface ISearchFilters
    {
        // PROPERTIES

        bool IsValid { get; }
        int Id { get; }
        bool ShowKeywordFilter { get; }
        bool ShowDateFilter { get; }
        bool ShowFilterList { get; }
        IList<string> Keywords { get; }
        DateTime From { get; }
        DateTime To { get; }
        IFilterList FilterList { get; }
        bool ShowAllResults { get; }
        int Page { get; }
    }
}
