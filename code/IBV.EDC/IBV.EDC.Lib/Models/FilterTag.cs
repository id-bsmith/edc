﻿using IBV.EDC.Lib.Models.Interfaces;

namespace IBV.EDC.Lib.Models
{
    public struct FilterTag : IFilterTag
    {
        // PROPERTIES

        public ITag Tag { get; set; }
        public bool IsSelected { get; set; }
    }
}
