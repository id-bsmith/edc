﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class TagList : ITagList
    {
        // PROPERTIES

        public bool IsValid => (Tags.Count > 0);
        public IList<Interfaces.ITag> Tags { get; private set; }


        // FACTORY

        public static ITagList Get(IEnumerable<IPublishedContent> contentList)
        {
            return new TagList(contentList);
        }


        // CTORS

        private TagList(IEnumerable<IPublishedContent> contentList)
        {
            Tags = new List<Interfaces.ITag>();

            LoadData(contentList);
        }


        // METHODS

        private void LoadData(IEnumerable<IPublishedContent> contentList)
        {
            if(contentList.Count() > 0)
            {
                foreach (var content in contentList)
                {
                    Interfaces.ITag tag = Tag.Get(content);
                    if (tag.IsValid)
                    {
                        Tags.Add(tag);
                    }
                }
            }
        }
    }
}