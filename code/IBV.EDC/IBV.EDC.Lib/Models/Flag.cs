﻿using IBV.EDC.Lib.Models.Interfaces;

namespace IBV.EDC.Lib.Models
{
    public class Flag : IFlag
    {
        // PROPERTIES

        public bool IsTrue { get; private set; }


        // FACTORY

        public static IFlag Get(string value)
        {
            return new Flag(value);
        }


        // CTORS

        private Flag(string value)
        {
            IsTrue = (value == "1" || value.ToLower() == "true");
        }
    }
}
