﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Models.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class ResultsPanel : IResultsPanel
    {
        // PROPERTIES

        public bool IsValid => Results.ResultCount > 0;
        public int Id { get; private set; }
        public string Heading { get; private set; }
        public ILink Link { get; private set; }
        public string LinkText { get; private set; }
        public ISearchResultList Results { get; private set; }


        // FACTORY

        public static IResultsPanel Get(IPublishedContent content)
        {
            // Search Results for ResultPanels are location on the resultsLink property
            // if this is unpublished it'll blow up.
            if (content?.Id > 0 && content.GetPropertyValue<IPublishedContent>("resultsLink") != null)
            {
                string cacheKey = string.Format("ResultsPanel-{0}", content.Id);

                IResultsPanel resultsPanel = CacheHelper.Get(cacheKey) as ResultsPanel;
                if (resultsPanel != null)
                {
                    return resultsPanel;
                }

                resultsPanel = new ResultsPanel(content);
                CacheHelper.Add(cacheKey, resultsPanel);
                return resultsPanel;
            }

            return null;
        }


        // CTORS

        private ResultsPanel(IPublishedContent content)
        {
            LoadData(content);
        }


        // METHODS

        private void LoadData(IPublishedContent content)
        {
            Id = content.Id;
            Heading = content.GetPropertyValue<string>("resultsHeading");
            Link = Models.Link.Get(content.GetPropertyValue<IPublishedContent>("resultsLink"));
            LinkText = content.GetPropertyValue<string>("resultsLinkText");
            Results = SearchResultList.Get(SearchSettings.Get(content, Link.Id));
        }
    }
}