﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class FaqGroupList : IFaqGroupList
    {
        // PROPERTIES

        public bool IsValid => FaqGroups.Count > 0;
        public IList<IFaqGroup> FaqGroups { get; private set; }


        // FACTORY

        public static IFaqGroupList Get(IEnumerable<IPublishedContent> contentList)
        {
            return new FaqGroupList(contentList);
        }


        // CTORS

        private FaqGroupList(IEnumerable<IPublishedContent> contentList)
        {
            FaqGroups = new List<IFaqGroup>();
            LoadData(contentList);
        }


        // METHODS

        private void LoadData(IEnumerable<IPublishedContent> contentList)
        {
            if(contentList.Count() > 0)
            {
                foreach(var content in contentList)
                {
                    IFaqGroup faqGroup = FaqGroup.Get(content);
                    if(faqGroup.IsValid)
                    {
                        FaqGroups.Add(faqGroup);
                    }
                }
            }            
        }
    }
}
