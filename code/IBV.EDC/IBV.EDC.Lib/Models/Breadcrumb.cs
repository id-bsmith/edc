﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class Breadcrumb : IBreadcrumb
    {
        // PROPERTIES

        public bool IsValid => Links.Count > 0;
        public IList<ILink> Links { get; private set; }


        // FACTORY

        public static IBreadcrumb Get()
        {
            string cacheKey = string.Format("Breadcrumb-{0}", IPublishedContentTools.GetCurrentNode().Id);

            IBreadcrumb breadcrumb = CacheHelper.Get(cacheKey) as Breadcrumb;
            if (breadcrumb != null)
            {
                return breadcrumb;
            }

            breadcrumb = new Breadcrumb();
            CacheHelper.Add(cacheKey, breadcrumb);
            return breadcrumb;
        }


        // CTORS

        private Breadcrumb()
        {
            Links = new List<ILink>();
            LoadData();
        }


        // METHODS

        private void LoadData()
        {
            IList<IPublishedContent> contentList = IPublishedContentTools.GetCurrentNode().AncestorsOrSelf<IPublishedContent>().ToList();
            if (contentList.Count > 0)
            {
                foreach (var content in contentList)
                {
                    if (content.Level >= IPublishedContentTools.HOME_NODE_PATH_LEVEL)
                    {
                        ILink link = Link.Get(content);
                        if (link.IsValid)
                        {
                           Links.Insert(0, link);
                        }
                    }
                }
            }
        }
    }
}
