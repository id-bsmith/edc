﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class FaqGroup : IFaqGroup
    {
        // PROPERTIES

        public bool IsValid => Faqs.Count > 0;
        public int Id { get; private set; }
        public string Heading { get; private set; }
        public IList<IFaq> Faqs { get; private set; }


        // FACTORY

        public static IFaqGroup Get(IPublishedContent content)
        {
            return new FaqGroup(content);
        }


        // CTORS

        private FaqGroup(IPublishedContent content)
        {
            Faqs = new List<IFaq>();
            LoadData(content);
        }


        // METHODS

        private void LoadData(IPublishedContent content)
        {
            if (content.Id > 0)
            {
                Id = content.Id;
                Heading = content.GetPropertyValue<string>("faqGroupHeading");

                foreach(var childContent in content.Children.Where(x => x.DocumentTypeAlias == "faq"))
                {
                    IFaq faq = Faq.Get(childContent);
                    if (faq.IsValid)
                    {
                        Faqs.Add(faq);
                    }
                }
            }
        }
    }
}
