﻿using Examine;
using Examine.LuceneEngine.SearchCriteria;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Core;

namespace IBV.EDC.Lib.Models
{
    public class SearchResultList : ISearchResultList
    {
        // VARIABLES

        private ISearchSettings _searchSettings;
        private ISearchFilters _searchFilters;
        private IEnumerable<Examine.SearchResult> _examineSearchResults = new List<Examine.SearchResult>();


        // PROPERTIES

        public bool IsValid => SearchResults.Count > 0;
        public IList<ISearchResult> SearchResults { get; private set; }
        public int PageCount { get; private set; }
        public int ResultCount { get; private set; }
        public string NoResultsText => _searchSettings.NoResultsText;


        // FACTORY

        public static ISearchResultList Get(ISearchSettings searchSettings, ISearchFilters searchFilters = null)
        {
            return new SearchResultList(searchSettings, searchFilters);
        }


        // CTORS

        private SearchResultList(ISearchSettings searchSettings, ISearchFilters searchFilters)
        {
            SearchResults = new List<ISearchResult>();
            _searchSettings = searchSettings;
            _searchFilters = searchFilters ?? SearchFilters.Get(null);

            LoadData();
        }


        // METHODS

        private void LoadData()
        {
            if (_searchSettings.IsValid)
            {
                // Load results
                switch (_searchSettings.Type)
                {
                    case Enums.SearchType.Content:
                        _examineSearchResults = _examineSearchResults.Concat(GetContentSearchResults());
                        break;
                    case Enums.SearchType.Pdfs:
                        _examineSearchResults = _examineSearchResults.Concat(GetPdfSearchResults());
                        break;
                    case Enums.SearchType.UsefulLinks:
                        _examineSearchResults = _examineSearchResults.Concat(GetUsefulLinkResults());
                        break;
                    case Enums.SearchType.Events:
                        _examineSearchResults = _examineSearchResults.Concat(GetEventSearchResults());
                        break;
                    default:
                        _examineSearchResults = _examineSearchResults.Concat(GetContentSearchResults());
                        _examineSearchResults = _examineSearchResults.Concat(GetPdfSearchResults());
                        break;
                }

                // Sort results
                switch (_searchSettings.Order)
                {
                    case Enums.SearchOrder.None:
                        _examineSearchResults = _examineSearchResults.OrderBy(x => Convert.ToInt32(x.Fields["sortOrder"]));
                        break;
                    case Enums.SearchOrder.Latest:
                        _examineSearchResults = _examineSearchResults.OrderByDescending(x => x.Fields["nodeDate"]);
                        break;
                    case Enums.SearchOrder.Oldest:
                        _examineSearchResults = _examineSearchResults.OrderBy(x => x.Fields["nodeDate"]);
                        break;
                    case Enums.SearchOrder.Alphabetical:
                        _examineSearchResults = _examineSearchResults.OrderBy(x => x.Fields["nodeTitle"]);
                        break;
                    case Enums.SearchOrder.Relevancy:
                    default:
                        break;
                }

                // Limit results
                if (_searchSettings.MaxResults > 0 && _examineSearchResults.Count() > _searchSettings.MaxResults)
                {
                    _examineSearchResults = _examineSearchResults.Take(_searchSettings.MaxResults);
                }

                // Save total number of results before the results are paged
                ResultCount = _examineSearchResults.Count();

                // Page results if required
                if (!_searchFilters.ShowAllResults)
                {
                    PageCount = (_examineSearchResults.Count() == 0 || _searchSettings.PageSize == 0) ? 0 : (((_examineSearchResults.Count() - 1) / _searchSettings.PageSize) + 1);
                    _examineSearchResults = _examineSearchResults.Skip((_searchFilters.Page - 1) * _searchSettings.PageSize).Take(_searchSettings.PageSize);
                }

                // Load Examine results into strongly typed object
                foreach (Examine.SearchResult examineSearchResult in _examineSearchResults)
                {
                    // The calls for 'link' and 'image' are quite brittle.  A few hacks inplace because Umbraco doesn't
                    // distinguish between media and content.  See Link model for more fun and games.

                    var link = examineSearchResult.Fields["__IndexType"] == "media"
                        ? Link.Get(IPublishedContentTools.GetUmbracoHelper().TypedMedia(examineSearchResult.Fields["id"]))
                        : Link.Get(IPublishedContentTools.GetUmbracoHelper().TypedContent(examineSearchResult.Fields["id"]));
                    var image = GuidUdi.TryParse(examineSearchResult.Fields["nodeImage"], out GuidUdi udi)
                        ? Image.Get(IPublishedContentTools.GetUmbracoHelper().TypedMedia(GuidUdi.Parse(examineSearchResult.Fields["nodeImage"])))
                        : Image.Get(null);
                    Guid[] tagList = !string.IsNullOrEmpty(examineSearchResult.Fields["nodeTags"])
                        ? examineSearchResult.Fields["nodeTags"].Split(',').Select(x => GuidUdi.Parse(x).Guid).ToArray()
                        : new Guid[0];

                    SearchResult searchResult = new SearchResult()
                    {
                        Id = examineSearchResult.Id,
                        Heading = examineSearchResult.Fields["nodeTitle"],
                        Author = examineSearchResult.Fields["nodeAuthor"],
                        Date = Date.Get(examineSearchResult.Fields["nodeDate"]),
                        Image = image,
                        Summary = examineSearchResult.Fields["nodeSummary"],
                        TagList = TagList.Get(IPublishedContentTools.GetUmbracoHelper().TypedContent(tagList)),
                        Link = link
                    };

                    SearchResults.Add(searchResult);
                }
            }
        }

        private List<Examine.SearchResult> GetContentSearchResults()
        {
            // Build criteria for content search (n.b. raw lucene queries should be added via criteria)
            var contentCriteria = ExamineManager.Instance.SearchProviderCollection["ContentSearcher"].CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);

            // Add node type criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.ValidNodeTypes.Count > 0)
            {
                StringBuilder validNodeTypeBuilder = new StringBuilder();
                validNodeTypeBuilder.Append("+__NodeTypeAlias: (");

                // Loop through valid node types. Note: These values should be OR-ed.
                foreach (string validNodeType in _searchSettings.ValidNodeTypes)
                {
                    validNodeTypeBuilder.Append(" ");
                    validNodeTypeBuilder.Append(validNodeType);
                }

                validNodeTypeBuilder.Append(")");
                contentCriteria = contentCriteria.RawQuery(validNodeTypeBuilder.ToString());
            }

            // Add search tags (OR) criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.FiltersOr.Count > 0)
            {
                StringBuilder filterOrBuilder = new StringBuilder();
                filterOrBuilder.Append("+searchableGuids: (");

                // Loop through pre-filters. Note: These pre-filter values should be OR-ed.
                foreach (Guid filterGuid in _searchSettings.FiltersOr)
                {
                    filterOrBuilder.Append(" ");
                    filterOrBuilder.Append(filterGuid);
                }

                filterOrBuilder.Append(")");
                contentCriteria = contentCriteria.RawQuery(filterOrBuilder.ToString());
            }

            // Add user filters criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchFilters.FilterList.Filters.Count > 0)
            {
                StringBuilder userFilterBuilder = new StringBuilder();

                // Loop through user filters. Note: User filter groups should be AND-ed.
                foreach (Filter filter in _searchFilters.FilterList.Filters)
                {
                    // Loop through values for filter. Note: User filter values should be OR-ed within single filter group.
                    bool containsTags = false;
                    foreach (IFilterTag filterTag in filter.FilterTags)
                    {
                        if (filterTag.IsSelected)
                        {
                            if (!containsTags)
                            {
                                userFilterBuilder.Append("+searchableGuids: (");
                                containsTags = true;
                            }

                            userFilterBuilder.Append(" ");
                            userFilterBuilder.Append(filterTag.Tag.Guid);
                        }
                    }

                    if (containsTags)
                    {
                        userFilterBuilder.Append(")");
                    }
                }

                if (userFilterBuilder.Length > 0)
                {
                    contentCriteria = contentCriteria.RawQuery(userFilterBuilder.ToString());
                }
            }

            // Build filter for content search (n.b. standard examine filters should be added via filter)
            var contentFilter = contentCriteria.Field("__IndexType", "content").Not().Field("hideFromSearch", "1");

            // Add search scope filter
            switch (_searchSettings.Scope)
            {
                case Enums.SearchScope.Descendants:
                    contentFilter = contentFilter.And().Field("searchablePath", string.Format(",{0},", _searchSettings.NodeId.ToString()));
                    contentFilter = contentFilter.Not().Field("nodeId", _searchSettings.NodeId.ToString());
                    break;

                case Enums.SearchScope.Children:
                    contentFilter = contentFilter.And().Field("parentNodeId", _searchSettings.NodeId.ToString());
                    break;

                case Enums.SearchScope.Site:
                    contentFilter = contentFilter.And().Field("siteNodeId", _searchSettings.SiteNodeId.ToString());
                    break;

                default:
                    break;
            }

            // Add search tags (AND) filter
            foreach (Guid filterGuid in _searchSettings.FiltersAnd)
            {
                contentFilter = contentFilter.And().Field("searchableGuids", filterGuid.ToString());
            }

            // Add search period filter
            switch (_searchSettings.Period)
            {
                case Enums.SearchPeriod.Past:
                    contentFilter = contentFilter.And().Range("nodeDate", DateTime.MinValue.ToString("yyyy-MM-ddTHH:mm:ss"), DateTime.Now.AddDays(1).ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
                    break;

                case Enums.SearchPeriod.Future:
                    contentFilter = contentFilter.And().Range("nodeDate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
                    break;

                default:
                    break;
            }

            // Add user keywords filters
            foreach (string keyword in _searchFilters.Keywords)
            {
                string sanitizedString = ExamineTools.SanitizeStringForLucene(keyword, _searchSettings.StopWords);
                if (!string.IsNullOrEmpty(sanitizedString))
                {
                    contentFilter = contentFilter.And().GroupedOr(_searchSettings.ContentSearchFields.ToArray(), sanitizedString.Fuzzy(_searchSettings.Fuzziness));
                }
            }

            // Add user date filters
            if (_searchFilters.From > DateTime.MinValue || _searchFilters.To < DateTime.MaxValue)
            {
                // If possible, add 1 day to upper search date to ensure items from same day are included
                DateTime manipulatedSearchTo = _searchFilters.To;
                if (manipulatedSearchTo < DateTime.MaxValue.AddDays(-1))
                {
                    manipulatedSearchTo = manipulatedSearchTo.AddDays(1);
                }

                contentFilter = contentFilter.And().Range("nodeDate", _searchFilters.From.ToString("yyyy-MM-ddTHH:mm:ss"), manipulatedSearchTo.ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
            }

            // Return content search results
            return ExamineManager.Instance.SearchProviderCollection["ContentSearcher"].Search(contentFilter.Compile()).ToList<Examine.SearchResult>();
        }

        private List<Examine.SearchResult> GetPdfSearchResults()
        {
            // Build criteria for PDF search (n.b. raw lucene queries should be added via criteria)
            var pdfCriteria = ExamineManager.Instance.SearchProviderCollection["PDFSearcher"].CreateSearchCriteria(UmbracoExamine.IndexTypes.Media);

            // Add node type criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.ValidNodeTypes.Count > 0)
            {
                StringBuilder validNodeTypeBuilder = new StringBuilder();
                validNodeTypeBuilder.Append("+__NodeTypeAlias: (");

                // Loop through valid node types. Note: These values should be OR-ed.
                foreach (string validNodeType in _searchSettings.ValidNodeTypes)
                {
                    validNodeTypeBuilder.Append(" ");
                    validNodeTypeBuilder.Append(validNodeType);
                }

                validNodeTypeBuilder.Append(")");
                pdfCriteria = pdfCriteria.RawQuery(validNodeTypeBuilder.ToString());
            }

            // Add search filters (OR) criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.FiltersOr.Count > 0)
            {
                StringBuilder filterOrBuilder = new StringBuilder();
                filterOrBuilder.Append("+searchableGuids: (");

                // Loop through pre-filters. Note: These pre-filter values should be OR-ed.
                foreach (Guid filterGuid in _searchSettings.FiltersOr)
                {
                    filterOrBuilder.Append(" ");
                    filterOrBuilder.Append(filterGuid);
                }

                filterOrBuilder.Append(")");
                pdfCriteria = pdfCriteria.RawQuery(filterOrBuilder.ToString());
            }

            // Add user filters criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchFilters.FilterList.Filters.Count > 0)
            {
                StringBuilder userFilterBuilder = new StringBuilder();

                // Loop through user filters. Note: User filter groups should be AND-ed.
                foreach (Filter filter in _searchFilters.FilterList.Filters)
                {
                    // Loop through values for filter. Note: User filter values should be OR-ed within single filter group.
                    bool containsTags = false;
                    foreach (IFilterTag filterTag in filter.FilterTags)
                    {
                        if (filterTag.IsSelected)
                        {
                            if (!containsTags)
                            {
                                userFilterBuilder.Append("+searchableGuids: (");
                                containsTags = true;
                            }

                            userFilterBuilder.Append(" ");
                            userFilterBuilder.Append(filterTag.Tag.Guid);
                        }
                    }

                    if (containsTags)
                    {
                        userFilterBuilder.Append(")");
                    }
                }

                if (userFilterBuilder.Length > 0)
                {
                    pdfCriteria = pdfCriteria.RawQuery(userFilterBuilder.ToString());
                }
            }

            // Build filter for PDF search (n.b. standard examine filters should be added via filter)
            var pdfFilter = pdfCriteria.Field("__IndexType", "media").Not().Field("hideFromSearch", "1");

            // Add search scope filter
            switch (_searchSettings.Scope)
            {
                // The Descendants and Children scopes cannot apply to PDFs as they are held in the media library
                case Enums.SearchScope.Descendants:
                case Enums.SearchScope.Children:
                case Enums.SearchScope.Site:
                    //pdfFilter = pdfFilter.And().Field("siteNodeId", _searchSettings.SiteNodeId.ToString());
                    break;

                default:
                    break;
            }

            // Add search tags (AND) filter
            foreach (Guid filterGuid in _searchSettings.FiltersAnd)
            {
                pdfFilter = pdfFilter.And().Field("searchableGuids", filterGuid.ToString());
            }

            // Add search period filter
            switch (_searchSettings.Period)
            {
                case Enums.SearchPeriod.Past:
                    pdfFilter = pdfFilter.And().Range("nodeDate", DateTime.MinValue.ToString("yyyy-MM-ddTHH:mm:ss"), DateTime.Now.AddDays(1).ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
                    break;

                case Enums.SearchPeriod.Future:
                    pdfFilter = pdfFilter.And().Range("nodeDate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
                    break;

                default:
                    break;
            }

            // Add user keywords filter
            foreach (string keyword in _searchFilters.Keywords)
            {
                string sanitizedString = ExamineTools.SanitizeStringForLucene(keyword, _searchSettings.StopWords);
                if (!string.IsNullOrEmpty(sanitizedString))
                {
                    pdfFilter = pdfFilter.And().GroupedOr(_searchSettings.PdfSearchFields.ToArray(), sanitizedString.Fuzzy(_searchSettings.Fuzziness));
                }
            }

            // Add user date filters
            if (_searchFilters.From > DateTime.MinValue || _searchFilters.To < DateTime.MaxValue)
            {
                // If possible, add 1 day to upper search date to ensure items from same day are included
                DateTime manipulatedSearchTo = _searchFilters.To;
                if (manipulatedSearchTo < DateTime.MaxValue.AddDays(-1))
                {
                    manipulatedSearchTo = manipulatedSearchTo.AddDays(1);
                }

                pdfFilter = pdfFilter.And().Range("nodeDate", _searchFilters.From.ToString("yyyy-MM-ddTHH:mm:ss"), manipulatedSearchTo.ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
            }

            // Return content search results
            return ExamineManager.Instance.SearchProviderCollection["PDFSearcher"].Search(pdfFilter.Compile()).ToList<Examine.SearchResult>();
        }

        private List<Examine.SearchResult> GetUsefulLinkResults()
        {
            // Build criteria for content search (n.b. raw lucene queries should be added via criteria)
            var usefulLinksCriteria = ExamineManager.Instance.SearchProviderCollection["UsefulLinksSearcher"].CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);

            // Add node type criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.ValidNodeTypes.Count > 0)
            {
                StringBuilder validNodeTypeBuilder = new StringBuilder();
                validNodeTypeBuilder.Append("+__NodeTypeAlias: (");

                // Loop through valid node types. Note: These values should be OR-ed.
                foreach (string validNodeType in _searchSettings.ValidNodeTypes)
                {
                    validNodeTypeBuilder.Append(" ");
                    validNodeTypeBuilder.Append(validNodeType);
                }

                validNodeTypeBuilder.Append(")");
                usefulLinksCriteria = usefulLinksCriteria.RawQuery(validNodeTypeBuilder.ToString());
            }

            // Add search tags (OR) criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.FiltersOr.Count > 0)
            {
                StringBuilder filterOrBuilder = new StringBuilder();
                filterOrBuilder.Append("+searchableGuids: (");

                // Loop through pre-filters. Note: These pre-filter values should be OR-ed.
                foreach (Guid filterGuid in _searchSettings.FiltersOr)
                {
                    filterOrBuilder.Append(" ");
                    filterOrBuilder.Append(filterGuid);
                }

                filterOrBuilder.Append(")");
                usefulLinksCriteria = usefulLinksCriteria.RawQuery(filterOrBuilder.ToString());
            }

            // Add user filters criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchFilters.FilterList.Filters.Count > 0)
            {
                StringBuilder userFilterBuilder = new StringBuilder();

                // Loop through user filters. Note: User filter groups should be AND-ed.
                foreach (Filter filter in _searchFilters.FilterList.Filters)
                {
                    // Loop through values for filter. Note: User filter values should be OR-ed within single filter group.
                    bool containsTags = false;
                    foreach (IFilterTag filterTag in filter.FilterTags)
                    {
                        if (filterTag.IsSelected)
                        {
                            if (!containsTags)
                            {
                                userFilterBuilder.Append("+searchableGuids: (");
                                containsTags = true;
                            }

                            userFilterBuilder.Append(" ");
                            userFilterBuilder.Append(filterTag.Tag.Guid);
                        }
                    }

                    if (containsTags)
                    {
                        userFilterBuilder.Append(")");
                    }
                }

                if (userFilterBuilder.Length > 0)
                {
                    usefulLinksCriteria = usefulLinksCriteria.RawQuery(userFilterBuilder.ToString());
                }
            }

            // Build filter for content search (n.b. standard examine filters should be added via filter)
            //var contentFilter = contentCriteria.Field("__IndexType", "content").Not().Field("hideFromSearch", "1");
            var usefulLinksFilter = usefulLinksCriteria.Field("__IndexType", "content");

            // Add search scope filter
            switch (_searchSettings.Scope)
            {
                // The Descendants and Children scopes cannot apply to PDFs as they are held in the media library
                case Enums.SearchScope.Descendants:
                case Enums.SearchScope.Children:
                case Enums.SearchScope.Site:
                    usefulLinksFilter = usefulLinksFilter.And().Field("parentID", IPublishedContentTools.GetUsefulLinksNode().Id.ToString());
                    break;

                default:
                    break;
            }

            // Add search tags (AND) filter
            foreach (Guid filterGuid in _searchSettings.FiltersAnd)
            {
                usefulLinksFilter = usefulLinksFilter.And().Field("searchableGuids", filterGuid.ToString());
            }

            // Return content search results
            return ExamineManager.Instance.SearchProviderCollection["UsefulLinksSearcher"].Search(usefulLinksFilter.Compile()).ToList<Examine.SearchResult>();
        }

        private List<Examine.SearchResult> GetEventSearchResults()
        {
            // Build criteria for content search (n.b. raw lucene queries should be added via criteria)
            var contentCriteria = ExamineManager.Instance.SearchProviderCollection["EventsSearcher"].CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);

            // Add node type criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.ValidNodeTypes.Count > 0)
            {
                StringBuilder validNodeTypeBuilder = new StringBuilder();
                validNodeTypeBuilder.Append("+__NodeTypeAlias: (");

                // Loop through valid node types. Note: These values should be OR-ed.
                foreach (string validNodeType in _searchSettings.ValidNodeTypes)
                {
                    validNodeTypeBuilder.Append(" ");
                    validNodeTypeBuilder.Append(validNodeType);
                }

                validNodeTypeBuilder.Append(")");
                contentCriteria = contentCriteria.RawQuery(validNodeTypeBuilder.ToString());
            }

            // Add search tags (OR) criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchSettings.FiltersOr.Count > 0)
            {
                StringBuilder filterOrBuilder = new StringBuilder();
                filterOrBuilder.Append("+searchableGuids: (");

                // Loop through pre-filters. Note: These pre-filter values should be OR-ed.
                foreach (Guid filterGuid in _searchSettings.FiltersOr)
                {
                    filterOrBuilder.Append(" ");
                    filterOrBuilder.Append(filterGuid);
                }

                filterOrBuilder.Append(")");
                contentCriteria = contentCriteria.RawQuery(filterOrBuilder.ToString());
            }

            // Add user filters criteria
            // Note: The Examine fluent API does not handle nested conditionals so we need to create a raw Lucene query
            if (_searchFilters.FilterList.Filters.Count > 0)
            {
                StringBuilder userFilterBuilder = new StringBuilder();

                // Loop through user filters. Note: User filter groups should be AND-ed.
                foreach (Filter filter in _searchFilters.FilterList.Filters)
                {
                    // Loop through values for filter. Note: User filter values should be OR-ed within single filter group.
                    bool containsTags = false;
                    foreach (IFilterTag filterTag in filter.FilterTags)
                    {
                        if (filterTag.IsSelected)
                        {
                            if (!containsTags)
                            {
                                userFilterBuilder.Append("+searchableGuids: (");
                                containsTags = true;
                            }

                            userFilterBuilder.Append(" ");
                            userFilterBuilder.Append(filterTag.Tag.Guid);
                        }
                    }

                    if (containsTags)
                    {
                        userFilterBuilder.Append(")");
                    }
                }

                if (userFilterBuilder.Length > 0)
                {
                    contentCriteria = contentCriteria.RawQuery(userFilterBuilder.ToString());
                }
            }

            // Build filter for content search (n.b. standard examine filters should be added via filter)
            var contentFilter = contentCriteria.Field("__IndexType", "content").Not().Field("hideFromSearch", "1");

            // Add search scope filter
            switch (_searchSettings.Scope)
            {
                case Enums.SearchScope.Descendants:
                    contentFilter = contentFilter.And().Field("searchablePath", string.Format(",{0},", _searchSettings.NodeId.ToString()));
                    contentFilter = contentFilter.Not().Field("nodeId", _searchSettings.NodeId.ToString());
                    break;

                case Enums.SearchScope.Children:
                    contentFilter = contentFilter.And().Field("parentNodeId", _searchSettings.NodeId.ToString());
                    break;

                case Enums.SearchScope.Site:
                    contentFilter = contentFilter.And().Field("siteNodeId", _searchSettings.SiteNodeId.ToString());
                    break;

                default:
                    break;
            }

            // Add search tags (AND) filter
            foreach (Guid filterGuid in _searchSettings.FiltersAnd)
            {
                contentFilter = contentFilter.And().Field("searchableGuids", filterGuid.ToString());
            }

            // Add search period filter
            switch (_searchSettings.Period)
            {
                case Enums.SearchPeriod.Past:
                    contentFilter = contentFilter.And().Range("nodeDate", DateTime.MinValue.ToString("yyyy-MM-ddTHH:mm:ss"), DateTime.Now.AddDays(1).ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
                    break;

                case Enums.SearchPeriod.Future:
                    contentFilter = contentFilter.And().Range("nodeDate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
                    break;

                default:
                    break;
            }

            // Add user keywords filters
            foreach (string keyword in _searchFilters.Keywords)
            {
                string sanitizedString = ExamineTools.SanitizeStringForLucene(keyword, _searchSettings.StopWords);
                if (!string.IsNullOrEmpty(sanitizedString))
                {
                    contentFilter = contentFilter.And().GroupedOr(_searchSettings.ContentSearchFields.ToArray(), sanitizedString.Fuzzy(_searchSettings.Fuzziness));
                }
            }

            // Add user date filters
            if (_searchFilters.From > DateTime.MinValue || _searchFilters.To < DateTime.MaxValue)
            {
                // If possible, add 1 day to upper search date to ensure items from same day are included
                DateTime manipulatedSearchTo = _searchFilters.To;
                if (manipulatedSearchTo < DateTime.MaxValue.AddDays(-1))
                {
                    manipulatedSearchTo = manipulatedSearchTo.AddDays(1);
                }

                contentFilter = contentFilter.And().Range("nodeDate", _searchFilters.From.ToString("yyyy-MM-ddTHH:mm:ss"), manipulatedSearchTo.ToString("yyyy-MM-ddTHH:mm:ss"), true, true);
            }

            // Return content search results
            return ExamineManager.Instance.SearchProviderCollection["EventsSearcher"].Search(contentFilter.Compile()).ToList<Examine.SearchResult>();
        }
    }
}