﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Extensions;
using System;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace IBV.EDC.Lib.Models
{
    public class Tag : Interfaces.ITag
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public Guid Guid { get; private set; }
        public int ParentId { get; private set; }
        public string Name { get; private set; }


        // FACTORY

        public static Interfaces.ITag Get(IPublishedContent content)
        {
            string cacheKey = string.Format("Tag-{0}", content.Id);

            Interfaces.ITag tag = CacheHelper.Get(cacheKey) as Tag;
            if (tag != null)
            {
                return tag;
            }

            tag = new Tag(content);
            CacheHelper.Add(cacheKey, tag);
            return tag;
        }


        // CTORS

        private Tag(IPublishedContent content)
        {
            LoadData(content);
        }


        // METHODS

        private void LoadData(IPublishedContent content)
        {
            if (content.Id > 0)
            {
                IsValid = true;
                Id = content.Id;
                Guid = content.GetGuid();
                ParentId = content.Parent != null ? content.Parent.Id : -1;
                Name = content.Name;
            }
        }
    }
}