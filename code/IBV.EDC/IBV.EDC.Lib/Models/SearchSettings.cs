﻿using IBV.EDC.Lib.Enums;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class SearchSettings : ISearchSettings
    {
        // CONSTANTS

        private const int DEFAULT_PAGE_SIZE = 10;
        private const float DEFAULT_FUZZINESS = 0.8f;
        private const int DEFAULT_MAX_RESULTS = 1000;
        private readonly string[] DEFAULT_CONTENT_SEARCH_FIELDS = new string[] { "nodeTitle", "nodeAuthor", "nodeSummary", "nodeHeading", "nodeContent", "metaTitle", "metaDescription" };
        private readonly string[] DEFAULT_PDF_SEARCH_FIELDS = new string[] { "nodeTitle", "nodeAuthor", "nodeSummary", "FileTextContent" };
        private readonly string[] DEFAULT_STOP_WORDS = new string[] { "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into",
                                                                      "is", "it", "no", "not", "of", "on", "or", "such", "that", "the", "their", "then",
                                                                      "there", "these", "they", "this", "to", "was", "will", "with" };


        // PROPERTIES

        public bool IsValid { get; private set; }
        public int NodeId { get; private set; }
        public int SiteNodeId { get; private set; }
        public SearchType Type { get; private set; }
        public SearchScope Scope { get; private set; }
        public IList<Guid> FiltersAnd { get; private set; }
        public IList<Guid> FiltersOr { get; private set; }
        public SearchPeriod Period { get; private set; }
        public SearchOrder Order { get; private set; }
        public int PageSize { get; set; }
        public string NoResultsText { get; private set; }
        public float Fuzziness { get; private set; }
        public int MaxResults { get; private set; }
        public IList<string> ContentSearchFields { get; private set; }
        public IList<string> PdfSearchFields { get; private set; }
        public IList<string> StopWords { get; set; }
        public IList<string> ValidNodeTypes { get; set; }
        public IList<int> ValidUserFilters { get; set; }


        // FACTORY
        /// <summary>
        /// optionally pass searchPageId to apply searchSettings to a specific page
        /// </summary>
        /// <param name="content"></param>
        /// <param name="searchPageId"></param>
        /// <returns></returns>
        public static ISearchSettings Get(IPublishedContent content, int searchPageId = -1)
        {
            string cacheKey = string.Format("SearchSettings-{0}", content.Id);

            ISearchSettings searchSettings = Caching.CacheHelper.Get(cacheKey) as SearchSettings;
            if (searchSettings != null)
            {
                return searchSettings;
            }

            searchSettings = new SearchSettings(content, searchPageId);
            Caching.CacheHelper.Add(cacheKey, searchSettings);
            return searchSettings;
        }


        // CTORS

        private SearchSettings(IPublishedContent content, int searchPageId)
        {
            LoadDefaults();
            LoadData(content, searchPageId);
        }


        // METHODS

        private void LoadDefaults()
        {
            FiltersAnd = new List<Guid>();
            FiltersOr = new List<Guid>();
            ValidNodeTypes = new List<string>();
            ValidUserFilters = new List<int>();
            Fuzziness = DEFAULT_FUZZINESS;
            MaxResults = DEFAULT_MAX_RESULTS;
            ContentSearchFields = new List<string>(DEFAULT_CONTENT_SEARCH_FIELDS);
            PdfSearchFields = new List<string>(DEFAULT_PDF_SEARCH_FIELDS);
            StopWords = new List<string>(DEFAULT_STOP_WORDS);
        }

        private void LoadData(IPublishedContent content, int searchPageId)
        {
            if (content.Id > 0)
            {
                IPublishedContent siteNode = IPublishedContentTools.GetSiteNode();
                if (siteNode?.Id > 0)
                {
                    IsValid = true;
                    NodeId = searchPageId == -1 ? content.Id : searchPageId;
                    SiteNodeId = siteNode.Id;

                    if (content.GetPropertyValue<string>("searchFiltersDocumentType")?.Length > 0)
                    {
                        ValidNodeTypes = content.GetPropertyValue<string>("searchFiltersDocumentType").ToLower().Split(',').ToList();
                    }

                    Enum.TryParse<SearchType>(content.GetPropertyValue<string>("searchType"), out SearchType searchType);
                    Type = searchType;

                    Enum.TryParse<SearchScope>(content.GetPropertyValue<string>("searchScope"), out SearchScope searchScope);
                    Scope = searchScope;

                    IEnumerable<Guid> searchFiltersAnd = content.GetPropertyValue<IEnumerable<IPublishedContent>>("searchFiltersAnd").Select(x => x.GetKey());
                    foreach (Guid filterGuid in searchFiltersAnd)
                    {
                        FiltersAnd.Add(filterGuid);
                    }

                    IEnumerable<Guid> searchFiltersOr = content.GetPropertyValue<IEnumerable<IPublishedContent>>("searchFiltersOr").Select(x => x.GetKey());
                    foreach (Guid filterGuid in searchFiltersOr)
                    {
                        FiltersOr.Add(filterGuid);
                    }

                    Enum.TryParse<SearchPeriod>(content.GetPropertyValue<string>("searchPeriod"), out SearchPeriod searchPeriod);
                    Period = searchPeriod;

                    Enum.TryParse<SearchOrder>(content.GetPropertyValue<string>("searchOrder"), out SearchOrder searchOrder);
                    Order = searchOrder;

                    int searchPageSize = DEFAULT_MAX_RESULTS;
                    int.TryParse(content.GetPropertyValue<string>("searchPageSize"), out searchPageSize);

                    PageSize = (searchPageSize == 0) ? DEFAULT_PAGE_SIZE : searchPageSize;
                    NoResultsText = content.GetPropertyValue<string>("searchNoResults");

                    IEnumerable<int> validUsersFilters = content.GetProperty("userFilters")?.GetValue<IEnumerable<IPublishedContent>>().Select(x => x.Id);
                    foreach (int filterId in validUsersFilters ?? Enumerable.Empty<int>())
                    {
                        ValidUserFilters.Add(filterId);
                    }
                }
            }
        }
    }
}
