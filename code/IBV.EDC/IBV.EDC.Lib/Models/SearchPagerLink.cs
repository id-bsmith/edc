﻿using IBV.EDC.Lib.Models.Interfaces;

namespace IBV.EDC.Lib.Models
{
    public struct SearchPagerLink : ISearchPagerLink
    {
        // PROPERTIES

        public int PageNumber { get; set; }
        public bool IsCurrent { get; set; }
    }
}
