﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Models.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class Widget : IWidget
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public string Code { get; private set; }


        // FACTORY

        public static IWidget Get(IPublishedContent content)
        {
            string cacheKey = string.Format("Widget-{0}", content.Id);

            IWidget widget = CacheHelper.Get(cacheKey) as Widget;
            if (widget != null)
            {
                return widget;
            }

            widget = new Widget(content);
            CacheHelper.Add(cacheKey, widget);
            return widget;
        }


        // CTORS

        private Widget(IPublishedContent content)
        {
            LoadData(content);
        }


        // METHODS

        private void LoadData(IPublishedContent content)
        {
            if (content.Id > 0)
            {
                IsValid = true;
                Id = content.Id;
                Code = content.GetPropertyValue<string>("code");
            }
        }
    }
}
