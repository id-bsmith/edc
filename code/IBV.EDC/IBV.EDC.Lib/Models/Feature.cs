﻿using IBV.EDC.Lib.Caching;
using IBV.EDC.Lib.Models.Interfaces;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class Feature : IFeature
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public string Heading { get; private set; }
        public IImage Image { get; private set; }
        public ILink Link { get; private set; }


        // FACTORY

        public static IFeature Get(IPublishedContent content)
        {
            string cacheKey = string.Format("Feature-{0}", content.Id);

            IFeature feature = CacheHelper.Get(cacheKey) as Feature;
            if (feature != null)
            {
                return feature;
            }

            feature = new Feature(content);
            CacheHelper.Add(cacheKey, feature);
            return feature;
        }


        // CTORS

        private Feature(IPublishedContent content)
        {
            LoadData(content);
        }


        // METHODS

        private void LoadData(IPublishedContent content)
        {
            if (content.Id > 0)
            {
                IsValid = true;
                Id = content.Id;
                Heading = content.GetPropertyValue<string>("featureHeading");
                Image = Models.Image.Get(content.GetPropertyValue<IPublishedContent>("featureImage"));
                Link = Models.Link.Get(content.GetPropertyValue<IPublishedContent>("featureLink"));
            }
        }
    }
}   