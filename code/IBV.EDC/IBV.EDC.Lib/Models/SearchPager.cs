﻿using IBV.EDC.Lib.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace IBV.EDC.Lib.Models
{
    public class SearchPager : ISearchPager
    {
        // CONSTANTS

        private const int PAGES_BEFORE_CURRENT = 5;
        private const int PAGES_AFTER_CURRENT = 5;


        // PROPERTIES

        public bool IsValid => (PagerLinks.Count > 0);
        public bool PreviousPageIsValid { get; private set; }
        public int PreviousPageNumber { get; private set; }
        public bool NextPageIsValid { get; private set; }
        public int NextPageNumber { get; private set; }
        public IList<ISearchPagerLink> PagerLinks { get; private set; }


        // FACTORY

        public static ISearchPager Get(int pageCount, int currentPage)
        {
            return new SearchPager(pageCount, currentPage);
        }


        // CTORS

        private SearchPager(int pageCount, int currentPage)
        {
            PagerLinks = new List<ISearchPagerLink>();
            LoadData(pageCount, currentPage);
        }


        // METHODS

        private void LoadData(int pageCount, int currentPage)
        {
            if (pageCount > 1)
            {
                PreviousPageNumber = currentPage - 1;
                NextPageNumber = currentPage + 1;

                int startPage = Math.Max(1, currentPage - PAGES_BEFORE_CURRENT);
                int endPage = Math.Min(pageCount, currentPage + PAGES_AFTER_CURRENT);

                if (currentPage <= PAGES_BEFORE_CURRENT)
                {
                    startPage = 1;
                    endPage = Math.Min(pageCount, PAGES_BEFORE_CURRENT + PAGES_AFTER_CURRENT + 1);
                }
                else if (currentPage > pageCount - PAGES_AFTER_CURRENT)
                {
                    startPage = Math.Max(1, pageCount - PAGES_AFTER_CURRENT - PAGES_BEFORE_CURRENT);
                    endPage = pageCount;
                }

                for (int i = startPage; i <= endPage; i++)
                {
                    ISearchPagerLink pagerLink = new SearchPagerLink()
                    {
                        PageNumber = i,
                        IsCurrent = (i == currentPage)
                    };

                    PagerLinks.Add(pagerLink);
                }
            }

            PreviousPageIsValid = (PreviousPageNumber > 0);
            NextPageIsValid = (NextPageNumber <= pageCount);
        }
    }
}
