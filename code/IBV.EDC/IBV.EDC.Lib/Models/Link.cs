﻿using IBV.EDC.Lib.Enums;
using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Lib.Models
{
    public class Link : ILink
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public LinkType Type { get; private set; }
        public string Url { get; private set; }
        public string Title { get; private set; }
        public IDate Date { get; private set; }
        public IImage Image { get; private set; }
        public string Summary { get; private set; }
        public IDictionary<string, string> Properties { get; private set; }


        // FACTORY

        public static ILink Get(IPublishedContent content)
        {
            if (content?.Id > 0)
            {
                string cacheKey = string.Format("Link-{0}", content.Id);

                ILink link = Caching.CacheHelper.Get(cacheKey) as Link;
                if (link != null)
                {
                    return link;
                }

                link = new Link(content);
                Caching.CacheHelper.Add(cacheKey, link);
                return link;
            }

            return new Link();
        }


        // CTORS

        public Link()
        {
            IsValid = false;
        }

        private Link(IPublishedContent content)
        {
            Properties = new Dictionary<string, string>();

            LoadNodeData(content);
        }


        // METHODS

        private void LoadNodeData(IPublishedContent content)
        {
            // Umbraco doesn't distinguish between media and content using IPublishedContent
            // we need to be able to find the difference between these two
            // and also check when docType usefulLinks so we can pull the URL from a diff property.
            // Possible workaround:  Check IPublishedContent for umbracoFile property?

            if (content.Id > 0)
            {
                IsValid = true;
                Id = content.Id;

                if (content.DocumentTypeAlias == "usefulLink")
                {
                    Type = LinkType.ExternalLink;
                    Url = content.GetPropertyValue<string>("linkURL");
                }
                else
                {
                    Type = content.DocumentTypeAlias == "externalLink" ? LinkType.ExternalLink : LinkType.InternalLink;
                    Url = Type == LinkType.ExternalLink ? content.GetPropertyValue<string>("linkURL") : content.Url;
                }

                Title = !string.IsNullOrWhiteSpace(content.GetPropertyValue<string>("nodeLinkTitle")) ? content.GetPropertyValue<string>("nodeLinkTitle") : content.Name;
                Date = Models.Date.Get(content.GetPropertyValue<string>("nodeDate"));
                Image = Models.Image.Get(content.GetPropertyValue<IPublishedContent>("nodeImage"));
                Summary = content.GetPropertyValue<string>("nodeSummary");
            }
        }
    }
}
