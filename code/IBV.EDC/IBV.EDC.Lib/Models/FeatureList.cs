﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class FeatureList : IFeatureList
    {
        // PROPERTIES

        public bool IsValid => Features.Count > 0;
        public IList<IFeature> Features { get; private set; }


        // FACTORY

        public static IFeatureList Get(IEnumerable<IPublishedContent> contentList)
        {
            return new FeatureList(contentList);
        }


        // CTORS

        private FeatureList(IEnumerable<IPublishedContent> contentList)
        {
            Features = new List<IFeature>();
            LoadData(contentList);
        }


        // METHODS

        private void LoadData(IEnumerable<IPublishedContent> contentList)
        {
            if (contentList.Count() > 0)
            {
                foreach(var content in contentList)
                {
                    IFeature feature = Feature.Get(content);
                    if (feature.IsValid)
                    {
                        Features.Add(feature);
                    }
                }
            }
        }
    }
}
