﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class FilterList : IFilterList
    {
        // PROPERTIES

        public bool IsValid => (Filters.Count > 0);
        public IList<IFilter> Filters { get; private set; }


        // FACTORY

        public static IFilterList Get(IEnumerable<IPublishedContent> contentList)
        {
            return new FilterList(contentList);
        }


        // CTORS

        private FilterList(IEnumerable<IPublishedContent> contentList)
        {
            Filters = new List<IFilter>();
            LoadData(contentList);
        }


        // METHODS

        private void LoadData(IEnumerable<IPublishedContent> contentList)
        {
            if (contentList.Count() > 0)
            {
                foreach (var content in contentList)
                {
                    IFilter filter = Filter.Get(content);
                    if (filter.IsValid)
                    {
                        Filters.Add(filter);
                    }
                }
            }
        }
    }
}
