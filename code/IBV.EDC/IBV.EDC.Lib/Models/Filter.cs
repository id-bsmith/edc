﻿using IBV.EDC.Lib.Models.Interfaces;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace IBV.EDC.Lib.Models
{
    public class Filter : IFilter
    {
        // PROPERTIES

        public bool IsValid { get; private set; }
        public int Id { get; private set; }
        public string Name { get; private set; }
        public IList<IFilterTag> FilterTags { get; set; }


        // FACTORY

        public static IFilter Get(IPublishedContent content)
        {
            // Note: Do not cache object as FilterTag selected values can be updated on the fly
            return new Filter(content);
        }


        // CTORS

        private Filter(IPublishedContent content)
        {
            FilterTags = new List<IFilterTag>();
            LoadData(content);
        }


        // METHODS

        private void LoadData(IPublishedContent content)
        {
            if(content.Id > 0)
            {
                IsValid = true;
                Id = content.Id;
                Name = content.Name;

                foreach (var childNode in content.Children)
                {
                    Interfaces.ITag tag = Tag.Get(childNode);
                    if (tag.IsValid)
                    {
                        FilterTags.Add(new FilterTag()
                        {
                            Tag = tag,
                            IsSelected = false
                        });
                    }
                }
            }
        }
    }
}
