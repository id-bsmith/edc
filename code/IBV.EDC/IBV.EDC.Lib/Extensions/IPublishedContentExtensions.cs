﻿using System;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Templates;

namespace IBV.EDC.Lib.Extensions
{
    public static class IPublishedContentExtensions
    {
        public static string GetParsedContent(this IPublishedContent content, string propertyName)
        {
            if (content.GetProperty(propertyName) != null)
            {
                return umbraco.library.RenderMacroContent(
                    TemplateUtilities.ParseInternalLinks(content.GetPropertyValue<string>(propertyName), UmbracoContext.Current.UrlProvider),
                    content.Id
                );
            }

            return null;
        }

        /// <summary>
        /// Gets the published content with a guaranteed key
        /// </summary>
        /// <param name="content">The published content</param>
        /// <returns>A published content with key</returns>
        /// <remarks>
        /// Workaround for the fact that GetKey() doesn't always work - see http://issues.umbraco.org/issue/U4-10128
        /// </remarks>
        public static Guid GetGuid(this IPublishedContent content)
        {
            if (content is IPublishedContentWithKey withKey)
                return withKey.Key;

            if (content is PublishedContentWrapped wrapped)
                return GetGuid(wrapped.Unwrap());

            return Guid.Empty;
        }
    }
}
