﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace IBV.EDC.Lib.Extensions
{
    public static class ObjectExtensions
    {
        // METHODS

        public static string GetDescriptionAttribute<T>(this T source)
        {
            if (source == null)
            {
                return null;
            }

            FieldInfo fieldInfo = source.GetType().GetRuntimeField(source.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes != null && attributes.Length > 0) ? attributes[0].Description : source.ToString();
        }

        public static string GetEnumFromDescription<T>(this string description)
        {
            var type = typeof(T);

            if (type.GetTypeInfo().IsEnum)
            {
                foreach (var field in type.GetRuntimeFields())
                {
                    DescriptionAttribute attribute = typeof(DescriptionAttribute).GetTypeInfo().GetCustomAttribute<DescriptionAttribute>();
                    if (attribute != null && attribute.Description == description)
                    {
                        return field.Name;
                    }
                }
            }

            return string.Empty;
        }
    }
}
