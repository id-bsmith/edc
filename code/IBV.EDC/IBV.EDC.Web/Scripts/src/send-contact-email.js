﻿$(function () {
    var sendContactEmail = function (e) {
        e.preventDefault();

        contactFormParams = {
            FullName: $('#FullName').val(),
            ContactNumber: $('#ContactNumber').val(),
            Email: $('#Email').val(),
            Message: $('#Message').val(),
            PageID: $("#PageID").val()
        };

        var errorHtml = "<div id=\"contact-form-error\">Something went wrong.  Please try again later.</div>";
        var successHtml = "<div id=\"contact-form-success\">Thank you for your enquiry.  We will respond to your enquiry as soon as possible.</div>";
        var serverErrorHtml = "<div id=\"contact-form-error\">An issue occured when submitted your form.</div>";

        $.ajax({
            type: 'POST',
            url: '/umbraco/Api/ContactFormApi/SendContactForm',
            data: JSON.stringify(contactFormParams),
            contentType: "application/json",
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $("#contact-form-error-wrapper").html(errorHtml);
            },
            beforeSend: function () { window.ajaxProcessing = true; /*ajaxLoaderIcon.show();*/ },
            complete: function () { /*ajaxLoaderIcon.hide();*/ window.ajaxProcessing = false; },
            statusCode: {
                200: function () {
                    $("#contact-form-success-wrapper").html(successHtml);
                },
                500: function () {
                    $("#contact-form-error-wrapper").html(serverErrorHtml);
                }
            }
        });
    };

    //$('#contact-submit').on('click', sendContactEmail);
});