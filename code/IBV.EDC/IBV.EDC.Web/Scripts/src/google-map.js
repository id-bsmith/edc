﻿$(function () {
    var BREAKPOINT_XS = 480;
    var BREAKPOINT_SM = 768;
    var BREAKPOINT_MD = 1200;
    var BREAKPOINT_LG = 1920;
    var mapRefresh;

    terratype.onLoad(function (provider, map) {
        // There doesn't seem to be an easy way to refresh the map using the Terratype API
        // so we save the map.refresh method and call this when on window resize event
        // The API does have an AutoRecenterAfterRefresh property but it doesn't fire on window resize.
        mapRefresh = map.refresh;

        initMap();
    });

    $(window).on('resize', function () {
        initMap();
        mapRefresh();
    });

    var initMap = function() {
        mapHeight = "400px";

        if (window.innerWidth > BREAKPOINT_XS) {
            mapHeight = "550px";
        }

        $('[id^="TerratypeGoogleMaps"]').height(mapHeight);
    };
});