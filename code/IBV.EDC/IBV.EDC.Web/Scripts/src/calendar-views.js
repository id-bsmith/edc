﻿$(function () {
    var calendarView = function() {
        if ($(window).width() < 769) {
            $('#calendar').fullCalendar('changeView', 'listMonth');
        }
        else {
            $('#calendar').fullCalendar('changeView', 'month');
        }
    }

    $(window).on('resize', calendarView);
    $(window).on('load', calendarView);
});