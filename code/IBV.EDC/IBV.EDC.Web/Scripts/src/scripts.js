﻿$(function () {

    var windowSize;
    var BREAKPOINT_XS = 480;
    var BREAKPOINT_SM = 768;
    var BREAKPOINT_MD = 1200;
    var BREAKPOINT_LG = 1920;

    var openHeaderSearch = function () {
        $('#header-search').css('display', 'flex');
        $('#keywords-header').focus();
    };

    var closeHeaderSearch = function () {
        $('#header-search').css('display', 'none');
    };

    var toggleSearchFilters = function () {
        var openClass = 'button--primary-open';
        var closeClass = 'button--primary-close';

        $('.list-filters__filters-wrapper').slideToggle();
        $('.list-filters__submit').toggleClass('open');

        if ($(this).hasClass(openClass)) {
            $(this).removeClass(openClass).addClass(closeClass);
        }
        else {
            $(this).removeClass(closeClass).addClass(openClass);
        }
    };

    var toggleMenu = function () {
        $('.main-menu-toggle-holder').toggleClass('open');

        if ($('.main-menu-toggle-holder').hasClass('open')) {
            $('#main-menu-wrapper').addClass('active');
        }
        else {
            $('#main-menu-wrapper').removeClass('active');
        }
    };

    var toggleDropdown = function () {
        event.preventDefault();
        if ($(this).parent('li').hasClass('hover')) {
            $(this).next('.dropdown').slideUp();
            $(this).parent('li').removeClass('hover');
        }
        else {
            $(this).next('.dropdown').slideDown();
            $(this).parent('li').addClass('hover');
        }
    };

    var windowResized = function () {
        if ($(document).width() > (BREAKPOINT_MD -1)) {
            $('.main-menu__wrapper').removeAttr('style');
            $('.main-menu-toggle-holder').removeClass('open');
        }
        if ($(document).width() > BREAKPOINT_SM) {
            $('.header-links').prependTo('.header__top-row');
            $('#header-search').insertAfter('.header-search__toggle');
        }
        else {
            $('#header-search').appendTo('#mobile-search');
            $('#header-search').removeAttr('style');
            $('.header-links').appendTo('#mobile-header-links');
        }
    };

    var faqs = function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).addClass('inactive');

            $(this).parent().removeClass('active');
            $(this).parent().addClass('inactive');

            $(this).next('.faq__body').slideUp('normal');
        }
        else {
            $('.faq__head').find('.active').removeClass('active');
            $(this).removeClass('inactive');
            $(this).addClass('active');

            $(this).parent().removeClass('inactive');
            $(this).parent().addClass('active');

            $(this).next('.faq__body').slideDown('normal');
        }
        event.preventDefault();
    };

    var scrollToMain = function () {
        event.preventDefault();
        if ($(window).width() > 1199) {
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top-140
            }, 500);
        }
        else {
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        }
    };

    var closeAlert = function () {
        event.preventDefault();
        expireCookie();
        $('.site-alert').css('display', 'none');
    };

    var closePopup = function () {
        event.preventDefault();
        $('.calendar-popup').css('display', 'none');
    };

    var expireCookie = function () {
        var cookieValue = Cookies.get('SiteAlertCookie');
        var updateDate = cookieValue.split('&')[1];

        if (updateDate.substr(0, 10) === "updateDate") {
            Cookies.set('SiteAlertCookie', "Expires=true&" + updateDate);
        }
    };

    var submitFilterForm = function () {

        var tagFilterForm = $('#tagfilterForm');

        if ($(this).attr('id') == 'tag-clear') {
            tagFilterForm.find('.taglist li input').each(function () {
                $(this).prop('checked', false);
            });
        }

        $(tagFilterForm).submit();
    };

    // Events
    $('.list-filters__toggle span').on('click', toggleSearchFilters);
    $('.main-menu-toggle').on('click', toggleMenu);
    $('.header-search__toggle').on('click', openHeaderSearch);
    $('.header-search__button--close').on('click', closeHeaderSearch);
    $('.more').on('click', toggleDropdown);
    $('.faq__head').on('click', faqs);
    $('.carousel__button').on('click', scrollToMain);
    $('.site-alert__close').on('click', closeAlert);
    $('.calendar-popup__close').on('click', closePopup);
    $('.filters-checkboxes [id^="tag-"]').on('click', submitFilterForm);
    $(window).on('resize', windowResized);
    $(window).on('load', windowResized);
});