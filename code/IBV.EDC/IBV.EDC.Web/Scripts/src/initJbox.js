﻿// IIFE - Immediately Invoked Function Expression
(function (yourcode) {

    // The global jQuery object is passed as a parameter
    yourcode(window.jQuery, window, document);

}(function ($, window, document) {

    // The $ is now locally scoped 
    $(function () {

        // The DOM is ready!
        new jBox("Image", { closeOnClick: 'box', preloadFirstImage: true});
    });

    // The rest of your code goes here!
    
}
));