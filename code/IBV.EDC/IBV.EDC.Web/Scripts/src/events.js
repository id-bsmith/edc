﻿$(document).ready(function () {
    var pageID = $("#pageID").data("val");
    var tagFilters = "";

    $(".filter-picker:checkbox:checked").each(function () {
        tagFilters += $(this).val() + ",";
    });

    $('#calendar').fullCalendar({
        views: {
            listMonth: {
                listDayFormat: 'D'
            },
        },
        events: {
            url: '/umbraco/Api/EventsApi/GetEventsData',
            type: 'GET',
            data: {
                pageid: pageID,
                tagFilters: tagFilters.substring(0, tagFilters.length - 1)
            },
            error: function () {
                alert('there was an error while fetching events!');
            }
        },
        timeFormat: 'H:mm',
        eventLimit: true,
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        eventRender: function (event, element) {
            element.attr('href', 'javascript:void(0);');
            element.click(function () {
                $("#event-url").html("");
                $("#event-time").html("");
                $("#event-day").html(moment(event.start).format('DD') + "<span>" + moment(event.start).format('MMM') + "</span>");
                $("#event-title").html(event.title);
                if (!event.allDay) {
                    $("#event-time").html(moment(event.start).format('h:mm') + " - " + moment(event.end).format('h:mma'));
                }
                $("#event-details").html(event.EventDetails);

                if (event.url != null) {
                    $("#event-url").html("<a href='" + event.url + "'>read more...</a>");
                }

                dialog = $("#calendar-popup").dialog({
                    autoOpen: false,
                    position: { my: "center", at: "center" },
                    height: 600,
                    width: 900
                });

                dialog.dialog("open");
            });
        }
    });


});

