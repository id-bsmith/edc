$(document).ready(function () {

    $owlContainer = $('.owl-carousel');
    $owlSlides = $owlContainer.children('div');
    $owlNav = $('.owl-nav');

    // More than one slide - initialize the carousel
    if ($owlSlides.length > 1) {
        $owlContainer.owlCarousel({
            navText: [$('.carousel__nav--prev'), $('.carousel__nav--next')],
            smartSpeed: 450,
            loop: true,
            margin: 0,
            autoplay: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 1,
                    nav: false
                }
            }
        });

    }
    else {
        $owlNav.hide();
    }

});