'use strict';

// -----------------------------------------
// There's no production tasks needed.
// Using ClientDependency will automatically minify CSS and JS if it's configured
// and used correctly.
//
//
// Dependencies
// -----------------------------------------------------------------------------

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var flatten = require('gulp-flatten');

var postcss = require("gulp-postcss");
var autoprefixer = require('autoprefixer');

// -----------------------------------------------------------------------------
// Configuration
// -----------------------------------------------------------------------------

var config = {
    distScriptDirectory: './Scripts/dist',
    vendorScriptDirectory: './Scripts/src/vendor',
    vendorCssDirectory: './content/css/vendor',
    jsFiles: [
        './Scripts/**/*.js',
        '!./Scripts/**/*.min.js'
    ],
    styleDirectory: './Content/css',
    scssFiles: './Sass/**/*.scss'
};

// ----------------------------------------------------------
// Install these scripts via NPM and they will automatically be moved
// to the correct location when the appropriate task is run.
//
// Note:  This project included the whole JQuery UI library during dev and the package in NPM only includes individual components which means
// it's unknown which libs need to be included post-dev.  Therefore, the css and js script is not added below but has been added to source control. 
//-----------------------------------------------------------

var scriptFilesToMove = [
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/js-cookie/src/js.cookie.js',
    './node_modules/moment/min/moment.min.js',
    './node_modules/fullcalendar/dist/fullcalendar.js',
    './node_modules/owl.carousel/dist/owl.carousel.min.js',
    './node_modules/jbox/source/jBox.min.js',
    './node_modules/picturefill/src/picturefill.js'
];

var cssFilesToMove = [
    './node_modules/fullcalendar/dist/fullcalendar.min.css'
];

// -----------------------------------------------------------------------------
// Tasks
// -----------------------------------------------------------------------------

gulp.task('styles', function () {
    return gulp
        .src(['Sass/themes/*.scss'])
        .pipe(sass({ outputStyle: 'expanded' })).on('error', sass.logError)
        .pipe(postcss([autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        })]))
        .pipe(gulp.dest(config.styleDirectory))
});

gulp.task('error-500', function () {
    return gulp
        .src('./Sass/error-500.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(concat('/error-500/error-500.css'))
        .pipe(gulp.dest(config.styleDirectory));
});

gulp.task('move-vendor-scripts', function () {
    gulp.src(scriptFilesToMove, { base: './' })
        .pipe(flatten())
        .pipe(gulp.dest(config.vendorScriptDirectory));
});

gulp.task('move-vendor-css', function () {
    gulp.src(cssFilesToMove, { base: './' })
        .pipe(flatten())
        .pipe(gulp.dest(config.vendorCssDirectory));
});

gulp.task('watch', function () {
    gulp.watch(config.scssFiles, ['styles']);
});


// -----------------------------------------------------------------------------
// Default task
// -----------------------------------------------------------------------------
gulp.task('development', ['move-vendor-scripts', 'move-vendor-css', 'styles', 'error-500', 'watch']);
gulp.task('build', ['move-vendor-scripts', 'move-vendor-css', 'styles', 'error-500']);