﻿using System.Collections.Generic;

namespace IBV.EDC.Web.ViewModels
{
    public class GroupedLinkListViewModel
    {
        public string Heading { get; set; }
        public List<CategoryLinkListViewModel> GroupedLinks { get; set; }
    }
}