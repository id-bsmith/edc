﻿namespace IBV.EDC.Web.ViewModels
{
    public class MetaDataViewModel
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTags { get; set; }
    }
}