﻿using IBV.EDC.Lib.Models.Interfaces;

namespace IBV.EDC.Web.ViewModels
{
    public class CategoryLinkListViewModel
    {
        public string Category { get; set; }
        public ILinkList GroupedLinks { get; set; }
    }
}