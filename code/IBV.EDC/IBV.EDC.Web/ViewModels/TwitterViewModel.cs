﻿using IBV.EDC.Lib.Domain.Twitter;
using System.Collections.Generic;

namespace IBV.EDC.Web.ViewModels
{
    public class TwitterViewModel
    {
        public string Heading { get; set; }
        public string AccountName { get; set; }
        public string AccountUrl { get; set; }
        public string FollowLinkText { get; set; }        
        public List<TwitterPost> Tweets { get; set; }
    }
}