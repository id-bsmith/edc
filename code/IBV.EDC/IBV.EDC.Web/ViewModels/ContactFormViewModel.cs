﻿using System.ComponentModel.DataAnnotations;

namespace IBV.EDC.Web.ViewModels
{
    public class ContactFormViewModel
    {
        [Display(Name = "Full Name")]
        [Required(ErrorMessage = "Please enter your full name")]
        [MaxLength(100, ErrorMessage = "100 characters maximum")]
        public string FullName { get; set; }

        [Display(Name = "Contact Number")]
        [Required(ErrorMessage = "Please enter your contact number")]
        public string ContactNumber { get; set; }

        [Display(Name = "Email")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Please enter a valid email address")]
        [Required(ErrorMessage = "Please enter your email")]
        [MaxLength(100, ErrorMessage = "100 characters maximum")]
        public string Email { get; set; }

        [Display(Name = "Message")]
        [Required(ErrorMessage = "Please enter your message")]
        public string Message { get; set; }
        public string PageID { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Please confirm you understand the our privacy policy and terms and conditions")]
        public bool TermsAndConditions { get; set; }

        public string PrivacyPolicyLabel { get; set; }
    }
}