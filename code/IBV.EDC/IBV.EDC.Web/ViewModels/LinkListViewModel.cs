﻿using IBV.EDC.Lib.Models;
using IBV.EDC.Lib.Models.Interfaces;

namespace IBV.EDC.Web.ViewModels
{
    public class LinkListViewModel
    {
        // PROPERTIES

        public bool IsValid => LinkList.IsValid;
        public string Title { get; set; }
        public ILinkList LinkList { get; set; }
    }
}