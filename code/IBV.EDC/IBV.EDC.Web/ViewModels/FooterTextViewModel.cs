﻿namespace IBV.EDC.Web.ViewModels
{
    public class FooterTextViewModel
    {
        public string Heading { get; set; }
        public string FooterText { get; set; }
    }
}