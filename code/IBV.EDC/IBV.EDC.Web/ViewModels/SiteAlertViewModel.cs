﻿namespace IBV.EDC.Web.ViewModels
{
    public class SiteAlertViewModel
    {
        public string AlertText { get; set; }
        public string AlertLinkText { get; set; }
        public string AlertLink { get; set; }
    }
}