﻿using IBV.EDC.Lib.Models;

namespace IBV.EDC.Web.ViewModels
{
    public class ImageCropViewModel
    {
        // PROPERTIES

        public bool IsValid => Image.IsValid;

        public Image Image { get; set; }
        public ImageCrop Large { get; set; }
        public ImageCrop Medium { get; set; }
        public ImageCrop Small { get; set; }
        public ImageCrop ExtraSmall { get; set; }
        public string Attributes { get; set; }
    }
}