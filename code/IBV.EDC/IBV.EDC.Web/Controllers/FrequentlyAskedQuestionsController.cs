﻿using IBV.EDC.Lib.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Web.Controllers
{
    public class FrequentlyAskedQuestionsController : BaseSurfaceController
    {
        public ActionResult FAQGroupList()
        {
            return PartialView("_FAQGroupList", FaqGroupList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("faqGroupList")));
        }
    }
}