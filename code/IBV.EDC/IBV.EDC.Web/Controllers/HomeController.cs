﻿using IBV.EDC.Lib.Domain.Twitter;
using IBV.EDC.Lib.Models;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using IBV.EDC.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Web.Controllers
{
    public class HomeController : BaseSurfaceController
    {
        [ChildActionOnly]
        public ActionResult Features()
        {
            return PartialView("_Features", FeatureList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("features")));
        }

        [ChildActionOnly]
        public ActionResult UpcomingEvents()
        {
            var pageID = UmbracoContext.Current.PageId;
            var node = Umbraco.TypedContent(pageID);

            var eventsNode = node.Children(x => x.DocumentTypeAlias == "listEvent").FirstOrDefault();
            if (eventsNode == null)
            {
                return null;
            }

            var calendarNode = eventsNode.Children.Where(x => x.DocumentTypeAlias == "calendar").FirstOrDefault();
            if (calendarNode == null || calendarNode.Id <= 0)
            {
                return null;
            }

            var calendarNodeID = calendarNode.Id;

            UpComingEvents homepageEvents = new UpComingEvents();
            int ResultCount = 0;

            IPublishedContent typedContentPicker = node.GetPropertyValue<IPublishedContent>("upcomingEvents");
            if (typedContentPicker != null)
            {
                homepageEvents.Heading = typedContentPicker.GetPropertyValue<string>("resultsHeading");
                homepageEvents.LinkText = typedContentPicker.GetPropertyValue<string>("resultsLinkText");
                IPublishedContent link = typedContentPicker.GetPropertyValue<IPublishedContent>("resultsLink");
                homepageEvents.Link = link != null ? link.Url : "";
                ResultCount = typedContentPicker.GetPropertyValue<int>("searchPageSize");
            }

            HttpResponseMessage r = IBV.EDC.Lib.Domain.EventsFeed.CalanderJson(calendarNodeID.ToString(), "");
            string json = r.Content.ReadAsStringAsync().Result;
            homepageEvents.Events = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Event>>(json);
            homepageEvents.Events = homepageEvents.Events.OrderBy(x => x.start).Take(ResultCount).ToList();

            return PartialView("_UpcomingEvents", homepageEvents);
            //return PartialView("_UpcomingEvents", ResultsPanel.Get(CurrentPage.GetPropertyValue<IPublishedContent>("upcomingEvents")));
        }

        [ChildActionOnly]
        public ActionResult LatestNews()
        {
            IResultsPanel model = ResultsPanel.Get(CurrentPage.GetPropertyValue<IPublishedContent>("latestNews"));

            if (model != null)
            {
                return PartialView("_LatestNews", model);
            }

            return null;
        }

        [ChildActionOnly]
        public ActionResult TwitterFeed()
        {
            var twitterNode = IPublishedContentTools.GetSocialMediaNode();
            var twitterScreenName = twitterNode.GetPropertyValue<string>("twitterAccountName");

            if (!string.IsNullOrWhiteSpace(twitterScreenName))
            {
                TwitterViewModel model = new TwitterViewModel()
                {
                    Heading = twitterNode.GetPropertyValue<string>("twitterHeading"),
                    FollowLinkText = twitterNode.GetPropertyValue<string>("twitterFollowLinkText"),
                    AccountUrl = $"https://twitter.com/{twitterScreenName}",
                    AccountName = $"@{twitterScreenName}",
                    Tweets = TwitterApi.GetUserTimeline(twitterScreenName, 1, twitterNode.GetPropertyValue<bool>("includeRetweets"), twitterNode.GetPropertyValue<bool>("excludeReplies"))
                };

                return PartialView("_Twitter", model);
            }

            return null;
        }
    }
}