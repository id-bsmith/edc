﻿using IBV.EDC.Lib.Models;
using IBV.EDC.Lib.Tools;
using IBV.EDC.Web.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Web.Mvc;
using Terratype;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Web.Controllers
{
    public class ContactFormController : BaseSurfaceController
    {
        // ACTIONS
        public ActionResult Contact()
        {
            var privUrl = Link.Get(IPublishedContentTools.GetConfigurationNode().GetPropertyValue<IPublishedContent>("siteCookiePolicyPage")).Url;
            var model = new ContactFormViewModel();

            model.PrivacyPolicyLabel = $"I understand the {IPublishedContentTools.GetHomeNode().Name} <a href='{ privUrl }'>privacy policy.</a>";

            return PartialView("_ContactForm", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContactPost(ContactFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    new EmailController().ContactEmail(model).Deliver();
                    TempData["EmailSuccess"] = true;
                }
                catch (Exception)
                {
                    ViewBag["EmailFailed"] = true;
                    return CurrentUmbracoPage();
                }

                return RedirectToCurrentUmbracoPage();
            }

            // Invalid model state
            return CurrentUmbracoPage();
        }

        [ChildActionOnly]
        public ActionResult GoogleMap()
        {
            // There's a bug in Terratype 1.0.16
            // https://our.umbraco.org/projects/backoffice-extensions/terratype/bugs-questions/90338-keynotfoundexception-in-publishedcache
            // As a workaround I've had to build the Options object manually which means hard-coding values but doubt these will need to change.

            dynamic json = JObject.Parse(CurrentPage.GetProperty("googleMap").DataValue.ToString());
            int zoom = (int)json.zoom;
            string[] positionLatLong = (string[])json.position.datum.ToString().Split(',');

            var googleMap = new Options
            {
                Provider = new Terratype.Providers.GoogleMapsV3()
                {
                   Variety = new Terratype.Providers.GoogleMapsV3.VarietyDefinition() { Basic = true },
                   ApiKey = ConfigurationManager.AppSettings["GoogleMapAPIKey"]
                },
                Icon = new Terratype.Models.Icon
                {
                    Url = new Uri($"{Request.Url.Scheme}://{Request.Url.Host}/content/images/map-marker.png"),
                    Size = new Terratype.Models.Icon.SizeDefinition { Width = 37, Height = 47 },
                    Anchor = new Terratype.Models.Icon.AnchorDefinition
                    {
                        Horizontal = new Terratype.Models.AnchorHorizontal(Terratype.Models.AnchorHorizontal.Style.Center),
                        Vertical = new Terratype.Models.AnchorVertical(Terratype.Models.AnchorVertical.Style.Center)
                    }
                },
                Height = 550,
                Zoom = zoom,
                Position = new Terratype.CoordinateSystems.Wgs84($"{positionLatLong[0]},{positionLatLong[1]}"),
                AutoRecenterAfterRefresh = true
            };

            return PartialView("_GoogleMap", googleMap);
        }
    }
}