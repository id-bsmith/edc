﻿using IBV.EDC.Lib.Domain;
using IBV.EDC.Lib.Models;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using IBV.EDC.Web.ViewModels;
using nuPickers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace IBV.EDC.Web.Controllers
{
    public class SharedController : BaseSurfaceController
    {
        [ChildActionOnly]
        public ActionResult BodyCloseCode()
        {
            return PartialView("_RawCode", LayoutNode.GetPropertyValue<string>("bodyCloseCode"));
        }

        [ChildActionOnly]
        public ActionResult BodyOpenCode()
        {
            return PartialView("_RawCode", LayoutNode.GetPropertyValue<string>("bodyOpenCode"));
        }

        [ChildActionOnly]
        public ActionResult BreadcrumbTrail()
        {
            return PartialView("_BreadcrumbTrail", Breadcrumb.Get());
        }

        [ChildActionOnly]
        public ActionResult Canonical()
        {
            return PartialView("_Canonical", IPublishedContentTools.GetCanonical(CurrentPage));
        }

        [ChildActionOnly]
        public ActionResult Carousel()
        {
            return PartialView("_Carousel", FeatureList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("carousel")));
        }

        [ChildActionOnly]
        public ActionResult CookieBar()
        {
            ILink model = Link.Get(ConfigurationNode.GetPropertyValue<IPublishedContent>("siteCookiePolicyPage"));
            return PartialView("_CookieBar", model);
        }

        [ChildActionOnly]
        public ActionResult DisplaySiteAlert()
        {
            var siteAlertNode = IPublishedContentTools.GetSiteAlertNode();
            if (SiteAlert.Display(siteAlertNode))
            {
                SiteAlertViewModel model = new SiteAlertViewModel()
                {
                    AlertText = siteAlertNode.GetPropertyValue<string>("siteAlertText"),
                    AlertLink = siteAlertNode.GetPropertyValue<string>("siteAlertLink"),
                    AlertLinkText = siteAlertNode.GetPropertyValue<string>("siteAlertLinkText")
                };

                return PartialView("_DisplaySiteAlert", model);
            }

            return null;
        }

        [ChildActionOnly]
        public ActionResult EnsureSSL()
        {
            IFlag useHTTPS = Flag.Get(ConfigurationManager.AppSettings.Get("umbracoUseSSL").ToString());
            if (useHTTPS.IsTrue && !HttpContext.Request.IsSecureConnection)
            {
                HttpContext.Response.RedirectPermanent("https://" + Request.Url.Host + Request.RawUrl);
            }

            if (!useHTTPS.IsTrue && HttpContext.Request.IsSecureConnection)
            {
                HttpContext.Response.RedirectPermanent("http://" + Request.Url.Host + Request.RawUrl);
            }

            return null;
        }

        [ChildActionOnly]
        public ActionResult FooterExternalLinks()
        {
            return PartialView("_FooterExternalLinks", LinkList.Get(LayoutNode.GetPropertyValue<IEnumerable<IPublishedContent>>("footerExternalLinks")));
        }

        [ChildActionOnly]
        public ActionResult FooterLinks()
        {
            return PartialView("_FooterLinks", LinkList.Get(LayoutNode.GetPropertyValue<IEnumerable<IPublishedContent>>("footerLinks")));
        }

        [ChildActionOnly]
        public ActionResult FooterLogo()
        {
            return PartialView("_FooterLogo", Image.Get(LayoutNode.GetPropertyValue<IPublishedContent>("footerLogo")));
        }

        [ChildActionOnly]
        public ActionResult FooterText()
        {
            var model = new FooterTextViewModel()
            {
                Heading = !String.IsNullOrWhiteSpace(LayoutNode.GetPropertyValue<string>("footerTextHeading")) ? LayoutNode.GetPropertyValue<string>("footerTextHeading") : "School Address",
                FooterText = LayoutNode.GetPropertyValue<string>("footerText")
            };

            return PartialView("_FooterText", model);
        }

        [ChildActionOnly]
        public ActionResult FooterCopyrightText()
        {
            return PartialView("_FooterCopyrightText", LayoutNode.GetPropertyValue<string>("headerLogoText"));
        }

        [ChildActionOnly]
        public ActionResult GoogleAnalytics()
        {
            return PartialView("_GoogleAnalytics", ConfigurationNode.GetPropertyValue<string>("googleAnalyticsKey"));
        }

        [ChildActionOnly]
        public ActionResult HeadAreaCode()
        {
            return PartialView("_RawCode", LayoutNode.GetPropertyValue<string>("headAreaCode"));
        }

        [ChildActionOnly]
        public ActionResult HeaderExternalLinks()
        {
            return PartialView("_HeaderExternalLinks", LinkList.Get(LayoutNode.GetPropertyValue<IEnumerable<IPublishedContent>>("headerExternalLinks")));
        }

        [ChildActionOnly]
        public ActionResult HeaderSearch()
        {
            ILink model = Link.Get(ConfigurationNode.GetPropertyValue<IPublishedContent>("siteSearchPage"));

            if (model != null)
            {
                return PartialView("_HeaderSearch", model);
            }

            return null;
        }

        [ChildActionOnly]
        public ActionResult HeaderLogo()
        {
            return PartialView("_HeaderLogo", Image.Get(LayoutNode.GetPropertyValue<IPublishedContent>("headerLogo")));
        }

        [ChildActionOnly]
        public ActionResult HeaderLogoText()
        {
            return PartialView("_HeaderLogoText", LayoutNode.GetPropertyValue<string>("headerLogoText"));
        }

        [ChildActionOnly]
        public ActionResult ImageCrop(Image image, ImageCrop lg, ImageCrop md, ImageCrop sm, ImageCrop xs, string attrs = "")
        {
            ImageCropViewModel model = new ImageCropViewModel()
            {
                Image = image,
                Large = lg,
                Medium = md,
                Small = sm,
                ExtraSmall = xs,
                Attributes = attrs
            };

            return PartialView("_ImageCrop", model);
        }

        [ChildActionOnly]
        public ActionResult ImageGallery()
        {
            return PartialView("_ImageGallery", ImageList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("ImageGallery")));
        }

        [ChildActionOnly]
        public ActionResult ImageLister()
        {
            return PartialView("_ImageLister", ImageList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("ImageGallery")));
        }

        [ChildActionOnly]
        public ActionResult LinkToParent()
        {
            return PartialView("_LinkToParent", Link.Get(CurrentPage.Parent));
        }

        [ChildActionOnly]
        public ActionResult MainNavigation()
        {
            return PartialView("_MainNavigation", Menu.Get(IPublishedContentTools.GetHomeNode(), isSideMenu: false));
        }

        [ChildActionOnly]
        public ActionResult MetaData()
        {
            MetaDataViewModel model = new MetaDataViewModel()
            {
                MetaTitle = String.Format("{0} - {1}", IPublishedContentTools.GetMetaTitle(CurrentPage), IPublishedContentTools.GetHomeNode().Name),
                MetaDescription = IPublishedContentTools.GetMetaDescription(CurrentPage),
                MetaTags = CurrentPage.GetPropertyValue<string>("metaTags")
            };

            return PartialView("_MetaData", model);
        }

        [ChildActionOnly]
        public ActionResult MetaRobots()
        {
            bool noIndex = ConfigurationNode.GetPropertyValue<bool>("noRobotsIndex");

            if (noIndex)
            {
                return PartialView("_MetaRobots", "<meta name=\"robots\" content=\"noindex, nofollow\" />");
            }

            return null;
        }

        [ChildActionOnly]
        public ActionResult PageBanner()
        {
            return PartialView("_PageBanner", Image.Get(CurrentPage.GetPropertyValue<IPublishedContent>("pageBanner")));
        }

        [ChildActionOnly]
        public ActionResult RelatedLinks()
        {
            LinkListViewModel model = new LinkListViewModel()
            {
                Title = CurrentPage.GetPropertyValue<string>("relatedLinksHeading"),
                LinkList = LinkList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("relatedLinks"))
            };

            return PartialView("_RelatedLinks", model);
        }

        [ChildActionOnly]
        public ActionResult RelatedMedia()
        {
            LinkListViewModel model = new LinkListViewModel()
            {
                Title = CurrentPage.GetPropertyValue<string>("relatedMediaHeading"),
                LinkList = LinkList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("relatedMedia"))
            };

            return PartialView("_RelatedMedia", model);
        }

        [ChildActionOnly]
        public ActionResult Stylesheet()
        {
            var stylesheetNodeId = LayoutNode.GetPropertyValue<Picker>("schoolStylesheet").SavedValue.ToString();
            var cssFileName = SchoolStylesheet.Get(stylesheetNodeId);

            return PartialView("_Stylesheet", cssFileName ?? "douglas-academy.css");
        }

        [ChildActionOnly]
        public ActionResult SubNavigation()
        {
            return PartialView("_SubNavigation", Menu.Get(ignoreRootHiddenStatus: true, isSideMenu: true));
        }
    }
}