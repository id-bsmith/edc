﻿using ActionMailerNext.Mvc5_2;
using IBV.EDC.Lib.Tools;
using IBV.EDC.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using Umbraco.Web;

namespace IBV.EDC.Web.Controllers
{
    public class EmailController : MailerBase
    {
        //private SmtpSection _smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

        public EmailResult ContactEmail(ContactFormViewModel contact)
        {
            var emailNode = IPublishedContentTools.GetEmailNodeFromID(contact.PageID);
            MailMessage msg = new MailMessage();

            try
            {
                MailAttributes.From = new MailAddress(msg.From.Address);
                AddCsvItemsToList(MailAttributes.To, emailNode.GetPropertyValue<string>("contactFormEmailToAddress"));
                MailAttributes.Subject = emailNode.GetPropertyValue<string>("contactFormEmailSubject");
                ViewBag.emailContent = emailNode.GetPropertyValue<string>("contactFormEmailContent");
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Email("ContactForm", contact);
        }

        private void AddCsvItemsToList(List<MailAddress> list, string csv)
        {
            foreach (string item in csv.Split(',').Select(x => x.Trim()).ToList())
            {
                list.Add(new MailAddress(item));
            }
        }
    }
}