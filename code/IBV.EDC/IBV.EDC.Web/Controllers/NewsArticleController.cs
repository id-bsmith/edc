﻿using IBV.EDC.Lib.Models;
using System;
using System.Web.Mvc;
using Umbraco.Web;

namespace IBV.EDC.Web.Controllers
{
    public class NewsArticleController : BaseSurfaceController
    {
        public ActionResult ArticleDate()
        {
            return PartialView("_ArticleDate", Date.Get(CurrentPage.GetPropertyValue<DateTime>("nodeDate")));
        }

        public ActionResult LinkToParent()
        {
            return PartialView("_LinkToParent", Link.Get(CurrentPage.Parent));
        }
    }
}