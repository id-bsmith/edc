﻿using Archetype.Models;
using IBV.EDC.Lib.Models;
using IBV.EDC.Web.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Linq;

namespace IBV.EDC.Web.Controllers
{
    public class GeneralArticleController : BaseSurfaceController
    {
        public ActionResult Faq()
        {
            return PartialView("_Faq", FaqList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("faq")));
        }

        public ActionResult Features()
        {
            return PartialView("_Features", FeatureList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("features")));
        }


        public ActionResult GroupedLinks(string linksPropertyName, string headingPropertyName)
        {
            GroupedLinkListViewModel model = new GroupedLinkListViewModel()
            {
                Heading = CurrentPage.GetPropertyValue<string>(headingPropertyName),
                GroupedLinks = CurrentPage.GetPropertyValue<ArchetypeModel>(linksPropertyName)
                                        .Fieldsets
                                        .Select(x => new CategoryLinkListViewModel
                                                {
                                                    Category = x.GetValue<string>("categoryHeading"),
                                                    GroupedLinks = LinkList.Get(x.GetValue<IEnumerable<IPublishedContent>>("categoryLinks"))
                                                })
                                        .ToList()
            };

            return PartialView("_GroupedLinks", model);
        }

        public ActionResult SideFeatures()
        {
            return PartialView("_SideFeatures", FeatureList.Get(CurrentPage.GetPropertyValue<IEnumerable<IPublishedContent>>("sideFeatures")));
        }
    }
}