﻿using IBV.EDC.Web.ViewModels;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using Umbraco.Web.WebApi;

namespace IBV.EDC.Web.Controllers.WebAPI
{
    public class ContactFormApiController : UmbracoApiController
    {
        [HttpPost]
        public HttpResponseMessage SendContactForm(ContactFormViewModel contact)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Invalid Form Data");
            }

            try
            {
                new EmailController().ContactEmail(contact).Deliver();
                
                return Request.CreateErrorResponse(HttpStatusCode.OK, "Success");

            }
            catch (Exception ex)
            {
                // We're screwed. Model state is valid but e-mail has failed.
                // Possibly need to configure e-mails properties with Umbraco.
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}