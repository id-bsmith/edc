﻿using IBV.EDC.Lib.Domain;
using System;
using System.Net.Http;
using System.Web.Mvc;
using Umbraco.Web.WebApi;

namespace IBV.EDC.Web.Controllers.WebAPI
{
    public class EventsApiController : UmbracoApiController
    {
        [HttpGet]
        public HttpResponseMessage GetEventsData(string pageID, string tagFilters)
        {
            try
            {
                tagFilters = tagFilters ?? "";
                return EventsFeed.CalanderJson(pageID, tagFilters);
            }
            catch(Exception ex)
            {
                string error = "{\"Error\": \"Error fetching data. " + ex.Message + "\"}";
                return new HttpResponseMessage()
                {
                    Content = new StringContent(error, System.Text.Encoding.UTF8, "application/json")
                };
            }
        }
    }
}