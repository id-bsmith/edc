﻿using IBV.EDC.Lib.Models;
using IBV.EDC.Lib.Models.Interfaces;
using IBV.EDC.Lib.Tools;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace IBV.EDC.Web.Controllers
{
    public class BaseSurfaceController : SurfaceController, IRenderMvcController
    {

        // PROPERTIES
        public IPublishedContent ConfigurationNode => IPublishedContentTools.GetConfigurationNode();
        public IPublishedContent LayoutNode => IPublishedContentTools.GetLayoutNode();

        public ISearchSettings SearchSettings
        {
            get
            {
                string key = "ListController-SearchSettings";

                ISearchSettings searchSettings = HttpContext.Items[key] as SearchSettings;
                if (searchSettings == null)
                {
                    searchSettings = IBV.EDC.Lib.Models.SearchSettings.Get(CurrentPage);
                    HttpContext.Items[key] = searchSettings;
                }

                return searchSettings;
            }
        }

        public ISearchFilters SearchFilters
        {
            get
            {
                string key = "ListController-SearchFilters";

                ISearchFilters searchFilters = HttpContext.Items[key] as SearchFilters;
                if (searchFilters == null)
                {
                    searchFilters = IBV.EDC.Lib.Models.SearchFilters.Get(CurrentPage);
                    HttpContext.Items[key] = searchFilters;
                }

                return searchFilters;
            }
        }

        public ISearchResultList SearchResultList
        {
            get
            {
                string key = "ListController-SearchResultList";

                ISearchResultList searchResultList = HttpContext.Items[key] as SearchResultList;
                if (searchResultList == null)
                {
                    searchResultList = IBV.EDC.Lib.Models.SearchResultList.Get(SearchSettings, SearchFilters);
                    HttpContext.Items[key] = searchResultList;
                }

                return searchResultList;
            }
        }

        public ISearchPager SearchPager
        {
            get
            {
                string key = "ListController-SearchPager";

                ISearchPager searchPager = HttpContext.Items[key] as SearchPager;
                if (searchPager == null)
                {
                    searchPager = IBV.EDC.Lib.Models.SearchPager.Get(SearchResultList.PageCount, SearchFilters.Page);
                    HttpContext.Items[key] = searchPager;
                }

                return searchPager;
            }
        }


        // ACTIONS

        public virtual ActionResult Index(RenderModel model)
        {
            return CurrentTemplate(model);
        }

        protected ActionResult CurrentTemplate<T>(T model)
        {
            var template = ControllerContext.RouteData.Values["action"].ToString();
            if (!EnsurePhsyicalViewExists(template))
            {
                return Content(string.Empty);
            }

            return View(template, model);
        }


        // METHODS

        protected bool EnsurePhsyicalViewExists(string template)
        {
            var result = ViewEngines.Engines.FindView(ControllerContext, template, null);
            if (result.View == null)
            {
                LogHelper.Warn<RenderMvcController>(string.Format("No physical template file was found for template: {0}", template));
                return false;
            }

            return true;
        }
    }
}