﻿using System.Web.Mvc;

namespace IBV.EDC.Web.Controllers
{
    public class ListController : BaseSurfaceController
    {
        [ChildActionOnly]
        public ActionResult ListFilters()
        {
            return PartialView("_ListFilters", SearchFilters);
        }

        [ChildActionOnly]
        public ActionResult ListFiltersCheckbox()
        {
            return PartialView("_ListFiltersCheckbox", SearchFilters);
        }

        [HttpPost]
        public ActionResult SubmitFiltersCheckbox(FormCollection formCollection)
        {
            return CurrentUmbracoPage();
        }

        [ChildActionOnly]
        public ActionResult ListFiltersSide()
        {
            return PartialView("_ListFiltersSide", SearchFilters);
        }

        [ChildActionOnly]
        public ActionResult ListResults()
        {
            return PartialView("_ListResults", SearchResultList);
        }

        [ChildActionOnly]
        public ActionResult ListResultsDocuments()
        {
            return PartialView("_ListResultsDocuments", SearchResultList);
        }

        [ChildActionOnly]
        public ActionResult ListResultsEvents()
        {
            return PartialView("_ListResultsEvents", SearchResultList);
        }

        [ChildActionOnly]
        public ActionResult ListResultsGrid(bool ignoreDates = false)
        {
            ViewBag.IgnoreDates = ignoreDates;
            return PartialView("_ListResultsGrid", SearchResultList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListResultsPost(FormCollection formCollection)
        {
            return CurrentUmbracoPage();
        }

        [ChildActionOnly]
        public ActionResult Pager()
        {
            return PartialView("_Pager", SearchPager);
        }
    }
}