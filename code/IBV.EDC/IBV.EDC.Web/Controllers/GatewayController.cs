﻿using System.Web.Mvc;

namespace IBV.EDC.Web.Controllers
{
    public class GatewayController : BaseSurfaceController
    {
        // ACTIONS
        public ActionResult GridResults()
        {
            return PartialView("_GridResults", SearchResultList);
        }
    }
}