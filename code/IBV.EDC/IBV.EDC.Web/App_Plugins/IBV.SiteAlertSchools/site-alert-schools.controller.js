﻿angular.module('umbraco').controller('SiteAlertSchools.Controller', function ($scope, appState, $filter, assetsService) {

    assetsService.loadCss('~/App_plugins/IBV.SiteAlertSchools/site-alert-schools.css');

    var selectedSchoolIds = $scope.model.value ? $scope.model.value : [];
    $scope.schools = [];

    var init = function () {
        // loop all 'website*' nodes in the content root and add the id and name to array
        // along with a checked true if id exists in selectedSchoolIds

        var content = appState.getTreeState('currentRootNode');
        content.root.children.forEach(function (item) {
            if (item.metaData.contentType != null && item.metaData.contentType.substring(0, 7) === 'website') {
                var obj = {};
                obj['id'] = item.id;
                obj['name'] = item.name;

                if (selectedSchoolIds.indexOf(item.id) > -1) {
                    obj['checked'] = true;
                }

                $scope.schools.push(obj);
            }
        });
    }

    $scope.toggleSchool = function () {
        $filter('filter')($scope.schools, { checked: true });
    };

    $scope.selectAll = function () {
        $scope.schools.forEach(function (x) {
            x.checked = true;
        });
    };

    $scope.removeAll = function () {
        $scope.schools.forEach(function (x) {
            x.checked = false;
        });
    };

    $scope.$on('formSubmitting', function () {
        $scope.model.value = $scope.schools.filter(x => x.checked == true).map(x => x.id);
    });

    // On load
    init();
});